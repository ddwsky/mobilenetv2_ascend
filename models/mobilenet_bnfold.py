import mindspore
import mindspore.context as context
import mindspore.nn as nn
from mindspore import Tensor, Parameter
from mindspore import Tensor
from mindspore.ops import operations as P
from mindinsight.profiler.profiling import Profiler

import os
import time
import numpy as np
import torch

from op_impl.nn_quant import Conv2dBatchNormQuant


def init_weight(w, min=-1., max=1.):
    w.set_parameter_data(np.random.uniform(min, max, w.data.shape()).astype('float32'))


if __name__ == "__main__":
    if torch.cuda.device_count() == 0:
        os.system("rm -rf kernel_meta")
        os.system("rm -rf /opt/npu/w00447567/workspace/tbe/impl/__pycache__")
        context.set_context(mode=context.GRAPH_MODE, device_target='Ascend', device_id=5)
        model = Conv2dBatchNormQuant(in_channels=3, out_channels=32, kernel_size=3, stride=2, pad_mode="same",
                                     eps=1e-5, momentum=0.9, quant_delay=100000, freeze_bn=100000, fake=False)
    else:
        context.set_context(mode=context.GRAPH_MODE, device_target='GPU', device_id=0)
        model = nn.Conv2dBatchNormQuant(in_channels=3, out_channels=32, kernel_size=3, stride=2, pad_mode="same",
                                        eps=1e-5, momentum=0.9, quant_delay=100000, freeze_bn=100000, fake=False)
    np.random.seed(0)
    np.set_printoptions(3)
    init_weight(model.weight, -10, 10)
    init_weight(model.gamma)
    init_weight(model.beta)
    init_weight(model.moving_mean)
    init_weight(model.moving_variance, 0., 1.)

    model.set_train()
    profiler = Profiler()
    for i in range(10):
        x = Tensor(np.random.uniform(-0.5, 1, size=[64, 3, 224, 224]).astype('float32'))
        start = time.time()
        outs = model(x)
        t = time.time() - start
        print("time={:.3}s".format(t))
    print(outs.shape())
    print(outs.asnumpy().flatten()[:6])  # [0.713 0.932 0.621 0.89  0.511 0.623]
    print(outs.asnumpy().flatten()[-6:])  # [-0.923 -0.925 -0.927 -0.92  -0.923 -0.922]
    profiler.analyse()
