import mindspore
import mindspore.context as context
import mindspore.nn as nn
from mindspore import Tensor, Parameter
from mindspore.common.initializer import initializer
from mindspore.ops import composite as C
from mindspore import Tensor
from mindspore.ops import operations as P
from mindspore.common.parameter import ParameterTuple
import numpy as np

from op_impl import batchnorm_fold, batchnorm_fold_grad
from op_impl.primitive import _BatchNormFoldGrad


class nn_BatchNormFoldGrad(nn.Cell):
    def __init__(self, epsilon=1e-5, is_training=True, freeze_bn=0):
        super(nn_BatchNormFoldGrad, self).__init__()
        self.is_gpu = context.get_context('device_target') == "GPU"
        if self.is_gpu:
            self.step = Parameter(initializer('zeros', [1], dtype=mindspore.int32), name='step', requires_grad=False)
            self.bn_grad = P.BatchNormFoldGrad(epsilon, is_training, freeze_bn)
        else:
            self.bn_grad = _BatchNormFoldGrad(epsilon, is_training, freeze_bn)

    def construct(self, d_batch_mean, d_batch_std, x, batch_mean, batch_std):
        if self.is_gpu:
            dx = self.bn_grad(d_batch_mean, d_batch_std, x, batch_mean, batch_std, self.step)
        else:
            dx = self.bn_grad(d_batch_mean, d_batch_std, x, batch_mean, batch_std)
        return dx


def test_batchnrom_fold2():
    num_features = 3
    epsilon = 1e-5
    is_training = True
    freeze_bn = 1000
    net = nn_BatchNormFoldGrad(epsilon, is_training, freeze_bn)
    net.set_train(False)

    np.random.seed(0)
    d_batch_mean = np.random.uniform(-0.5, 1, (num_features,)).astype('float32')
    d_batch_std = np.random.uniform(-0.5, 1, (num_features,)).astype('float32')
    x = np.random.uniform(-0.5, 1, size=[3, num_features, 32, 32]).astype('float32')
    batch_mean = np.random.uniform(-0.5, 1, (num_features,)).astype('float32')
    batch_std = np.random.uniform(-0.5, 1, (num_features,)).astype('float32')

    dx = net(Tensor(d_batch_mean), Tensor(d_batch_std), Tensor(x), Tensor(batch_mean), Tensor(batch_std))
    print(dx.asnumpy().flatten()[:10])


context.set_context(mode=context.GRAPH_MODE, device_target='Ascend', device_id=3)
# context.set_context(mode=context.GRAPH_MODE, device_target='GPU', device_id=0)
test_batchnrom_fold2()
