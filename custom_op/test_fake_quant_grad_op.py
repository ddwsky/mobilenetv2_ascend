# Copyright 2020 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================

import numpy as np
import pytest
from mindspore import Tensor
import mindspore.nn as nn
from mindspore.common.api import ms_function
from mindspore.ops import operations as P
import mindspore.context as context

context.set_context(device_target='Ascend', device_id=0)


class Net(nn.Cell):
    def __init__(self, num_bits=8, quant_delay=0):
        from op_impl import fake_quant_with_min_max_var_ema_grad

        super(Net, self).__init__()
        self.op = P.FakeQuantWithMinMaxGrad(num_bits=8, quant_delay=0)

    @ms_function
    def construct(self, dout, x, min, max):
        return self.op(dout, x, min, max)


def numpy_result(dout, x, min, max, num_bits=8):
    quant_min = 0
    quant_max = 2 ** num_bits - 1
    scale = (max - min) / (quant_max - quant_min)
    zp_from_min = quant_min - (min / scale)
    # Nudge zero pointv
    nudge_zp = np.round(np.minimum(quant_max, np.maximum(quant_min, zp_from_min)))
    nudge_min = (quant_min - nudge_zp) * scale
    nudge_max = (quant_max - nudge_zp) * scale
    dx = np.where(np.logical_and(x >= nudge_min[0], x <= nudge_max[0]), dout, np.zeros_like(dout))
    return dx, nudge_min, nudge_max


def _test_fake_quant_grad(param):
    num_bits, *shapes = param
    net = Net(num_bits=num_bits)
    dout = np.ones(shape=[*shapes]).astype('float32')
    x = np.random.uniform(-1, 1, size=[*shapes]).astype('float32')
    min = np.random.uniform(-1, 0, size=[1]).astype('float32')
    max = np.random.uniform(0, 1, size=[1]).astype('float32')

    output = net(Tensor(dout), Tensor(x), Tensor(min), Tensor(max))
    expect, nudge_min, nudge_max = numpy_result(dout, x, min, max)
    try:
        assert (np.allclose(output.asnumpy(), expect, rtol=1.e-5, atol=1.e-5))
    except AssertionError:
        # print("=" * 20, "output", "=" * 20)
        # print(output.asnumpy())
        # print("=" * 20, "expect", "=" * 20)
        # print(expect)
        print("=" * 20, "diff", "=" * 20)
        diff = output.asnumpy() - expect
        print(x[diff != 0])
        print("min", nudge_min, "max", nudge_max)
        raise


def test_fake_quant_grad():
    # num_bits, *shapes
    param_list = (
        [8, 256, 24, 56, 56],
        [8, 256, 64, 14, 14],
        [8, 256, 160, 7, 7],
        [8, 256, 96, 56, 56],
        [8, 256, 96, 14, 14],
        [8, 256, 192, 14, 14],
        [8, 256, 384, 14, 14],
        [8, 256, 144, 28, 28],
        [8, 256, 16, 112, 112],
        [8, 256, 192, 28, 28],
        [8, 256, 32, 112, 112],
        [8, 256, 1280, 7, 7],
        [8, 256, 96, 112, 112],
        [8, 256, 144, 56, 56],
        [8, 256, 320, 7, 7],
        [8, 256, 960, 7, 7],
        [8, 256, 32, 28, 28],
        [8, 256, 576, 14, 14],
        [8, 256, 576, 7, 7],
    )
    for i in param_list:
        _test_fake_quant_grad(i)
