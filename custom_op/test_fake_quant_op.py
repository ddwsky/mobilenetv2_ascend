# Copyright 2020 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================

import numpy as np
import pytest
from mindspore import Tensor
from mindspore import nn
import mindspore.context as context
from mindspore.common import dtype as mstype
from mindspore.common.api import ms_function
from op_impl.nn_quant import FakeQuantWithMinMaxD, FakeQuantWithMinMax

context.set_context(device_target='Ascend', device_id=0)

def fake_quant_np(x, min_val, max_val, quant_min, quant_max, ema, ema_decay):
    # CalMinMax
    input_min = np.min(x)
    input_max = np.max(x)
    if ema:
        input_min = ema_decay * min_val + (1 - ema_decay) * input_min
        input_min = 0 if input_min > 0 else input_min
        input_max = ema_decay * max_val + (1 - ema_decay) * input_max
        input_max = 0 if input_max < 0 else input_max
    # CalNudge
    if input_min == input_max:
        scale = 0
        zp_from_min = 0
    else:
        scale = (input_max - input_min)/(quant_max - quant_min)
        zp_from_min = quant_min - input_min/scale
    if zp_from_min <= quant_min:
        nudge_zp = quant_min
    elif zp_from_min >= quant_max:
        nudge_zp = quant_max
    else:
        nudge_zp = round(zp_from_min)
    nudge_min = np.float32((quant_min - nudge_zp) * scale)
    nudge_max = np.float32((quant_max - nudge_zp) * scale)
    out = np.minimum(np.maximum(x, nudge_min), nudge_max)
    nudge_input = np.floor(out - nudge_min)/scale + 0.5
    res = nudge_input * scale + nudge_min
    return [res, input_min, input_max]

class Net(nn.Cell):
    def __init__(self,
                 min_init=-6,
                 max_init=6,
                 num_bits=8,
                 ema=False,
                 ema_decay=0.999,
                 per_channel=False,
                 channel_size=1,
                 quant_delay=0,
                 symmetric=False,
                 narrow_range=False,
                 training=True):
        from op_impl import fake_quant_with_min_max_var_ema
        super(Net, self).__init__()
        self.fake_quant = FakeQuantWithMinMaxD(num_bits=num_bits,
                                               ema=ema,
                                               ema_decay=ema_decay,
                                               quant_delay=quant_delay,
                                               symmetric=symmetric,
                                               narrow_range=narrow_range,
                                               training=training)

        self.min = self.fake_quant.min
        self.max = self.fake_quant.max

    @ms_function
    def construct(self, input, min, max):
        return self.fake_quant(input, min, max)

def _test_fake_quant(shape,
                     min_val=-6.,
                     max_val=6.,
                     traning=True,
                     ema=True,
                     ema_decay=0.999,
                     symmetric=False,
                     narrow_range=False,
                     num_bits=8):
    x = np.random.uniform(3, 10, size=shape)
    # min_val = -6
    # max_val = 6
    # training = True
    # ema = True
    # ema_decay = 0.999
    if symmetric:
        quant_min = 0 - 2 ** (num_bits - 1)
        quant_min = 2 ** (num_bits-1) - 1
    else:
        quant_min = 0
        quant_max = 2 ** num_bits - 1
    if narrow_range:
        quant_min = quant_min + 1
    net = Net(training=False, min_init=min_val, max_init=max_val, ema=ema, ema_decay=ema_decay)
    print("fake min before: {}".format(net.min.data))
    print("fake max before: {}".format(net.max.data))
    output = net(Tensor(x), Tensor([min_val], dtype=mstype.float32), Tensor([max_val], dtype=mstype.float32))
    expect, min_np, max_np = fake_quant_np(x, min_val, max_val, quant_min, quant_max, ema=ema, ema_decay=ema_decay)
    error = np.ones(shape=expect.shape) * 1.0e-5
    diff = output.asnumpy() - expect
    print("output: ", output)
    print("expect: ", expect)
    print("fake min after: {}".format(net.min.data))
    print("fake max after: {}".format(net.max.data))
    assert np.all(np.abs(diff) < error)

def test_fake_quant3():
    x = np.array([-0.1, -0.0, -0.1, 0.25, 63.75, 63.8]).reshape(1, 1, 1, 6).astype(np.float32)
    min_val = -0.1
    max_val = 63.65
    net = Net(training=False, min_init=min_val, max_init=max_val)
    print("fake min before: {}".format(net.min.data))
    print("fake max before: {}".format(net.max.data))
    output = net(Tensor(x), Tensor([min_val], dtype=mstype.float32), Tensor([max_val], dtype=mstype.float32))
    expect = np.array([-0.0,0.0,0.0,0.25,63.75,63.75]).astype(np.float32)
    error = np.ones(shape=expect.shape) * 1.0e-5
    diff = output.asnumpy() - expect
    print("output: ", output)
    print("expect: ", expect)
    print("fake min after: {}".format(net.min.data))
    print("fake max after: {}".format(net.max.data))
    assert np.all(np.abs(diff) < error)

def test_fake_quant4():
    x = np.array([-0.26, -0.25, -0.24, 0.0, 63.5, 63.6]).reshape(1, 1, 1, 6).astype(np.float32)
    min_val = -0.125
    max_val = 63.625
    net = Net(training=False, min_init=min_val, max_init=max_val)
    print("fake min before: {}".format(net.min.data))
    print("fake max before: {}".format(net.max.data))
    output = net(Tensor(x), Tensor([min_val], dtype=mstype.float32), Tensor([max_val], dtype=mstype.float32))
    expect = np.array([-0.25, -0.25, -0.25, 0.0, 63.5, 63.5]).astype(np.float32)
    error = np.ones(shape=expect.shape) * 1.0e-5
    diff = output.asnumpy() - expect
    print("output: ", output)
    print("expect: ", expect)
    print("fake min after: {}".format(net.min.data))
    print("fake max after: {}".format(net.max.data))
    assert np.all(np.abs(diff) < error)

def test_fake_quant5():
    x = np.array([-63.8, -63.75, -63.7, -63.5, 0.0, 0.1]).reshape(1, 1, 1, 6).astype(np.float32)
    min_val = -63.65
    max_val = 0.1
    net = Net(training=False, min_init=min_val, max_init=max_val)
    print("fake min before: {}".format(net.min.data))
    print("fake max before: {}".format(net.max.data))
    output = net(Tensor(x), Tensor([min_val], dtype=mstype.float32), Tensor([max_val], dtype=mstype.float32))
    expect = np.array([-63.75, -63.75, -63.75, -63.5, 0.0, 0.0]).astype(np.float32)
    error = np.ones(shape=expect.shape) * 1.0e-5
    diff = output.asnumpy() - expect
    print("output: ", output)
    print("expect: ", expect)
    print("fake min after: {}".format(net.min.data))
    print("fake max after: {}".format(net.max.data))
    assert np.all(np.abs(diff) < error)

def test_fake_quant6():
    x = np.array([-6.1, -6.0, -5.9, -5.5, 1.5, 1.6]).reshape(1, 1, 1, 6).astype(np.float32)
    min_val = -6.0
    max_val = 1.5
    net = Net(training=False, min_init=min_val, max_init=max_val)
    print("fake min before: {}".format(net.min.data))
    print("fake max before: {}".format(net.max.data))
    output = net(Tensor(x), Tensor([min_val], dtype=mstype.float32), Tensor([max_val], dtype=mstype.float32))
    expect = np.array([-6.0, -6.0, -6.0, -5.5, 1.5, 1.5]).astype(np.float32)
    error = np.ones(shape=expect.shape) * 1.0e-5
    diff = output.asnumpy() - expect
    print("output: ", output)
    print("expect: ", expect)
    print("fake min after: {}".format(net.min.data))
    print("fake max after: {}".format(net.max.data))
    assert np.all(np.abs(diff) < error)

def test_fake_quant7():
    x = np.array([-0.1, -0.0, 0.1, 0.5, 7.5, 7.6]).reshape(1, 1, 1, 6).astype(np.float32)
    min_val = -0.1
    max_val = 7.4
    net = Net(training=False, min_init=min_val, max_init=max_val)
    print("fake min before: {}".format(net.min.data))
    print("fake max before: {}".format(net.max.data))
    output = net(Tensor(x), Tensor([min_val], dtype=mstype.float32), Tensor([max_val], dtype=mstype.float32))
    expect = np.array([-0.0, 0.0, 0.0, 0.5, 7.5, 7.5]).astype(np.float32)
    error = np.ones(shape=expect.shape) * 1.0e-5
    diff = output.asnumpy() - expect
    print("output: ", output)
    print("expect: ", expect)
    print("fake min after: {}".format(net.min.data))
    print("fake max after: {}".format(net.max.data))
    assert np.all(np.abs(diff) < error)

def test_fake_quant8():
    x = np.array([-0.6, -0.5, -0.24, 0.0, 7.0, 7.1]).reshape(1, 1, 1, 6).astype(np.float32)
    min_val = -0.4
    max_val = 7.1
    net = Net(training=False, min_init=min_val, max_init=max_val)
    print("fake min before: {}".format(net.min.data))
    print("fake max before: {}".format(net.max.data))
    output = net(Tensor(x), Tensor([min_val], dtype=mstype.float32), Tensor([max_val], dtype=mstype.float32))
    expect = np.array([-0.5, -0.5, 0.0, 0.0, 7.0, 7.0]).astype(np.float32)
    error = np.ones(shape=expect.shape) * 1.0e-5
    diff = output.asnumpy() - expect
    print("output: ", output)
    print("expect: ", expect)
    print("fake min after: {}".format(net.min.data))
    print("fake max after: {}".format(net.max.data))
    assert np.all(np.abs(diff) < error)

def test_fake_quant9():
    x = np.array([-7.611, -7.5222, -7.433333, 7.21111, 0.08888, 0.1]).reshape(1, 1, 1, 6).astype(np.float32)
    min_val = -7.3
    max_val = 0.2
    net = Net(training=False, min_init=min_val, max_init=max_val)
    print("fake min before: {}".format(net.min.data))
    print("fake max before: {}".format(net.max.data))
    output = net(Tensor(x), Tensor([min_val], dtype=mstype.float32), Tensor([max_val], dtype=mstype.float32))
    expect = np.array([-7.5, -7.5, -7.5, -7, 0.0, 0.0]).astype(np.float32)
    error = np.ones(shape=expect.shape) * 1.0e-5
    diff = output.asnumpy() - expect
    print("output: ", output)
    print("expect: ", expect)
    print("fake min after: {}".format(net.min.data))
    print("fake max after: {}".format(net.max.data))
    assert np.all(np.abs(diff) < error)


def test_fake_quant_main():
    # num_bits, *shapes
    param_list = (
        [8, 256, 24, 56, 56],
        [8, 256, 64, 14, 14],
        [8, 256, 160, 7, 7],
        [8, 256, 96, 56, 56],
        [8, 256, 96, 14, 14],
        [8, 256, 192, 14, 14],
        [8, 256, 384, 14, 14],
        [8, 256, 144, 28, 28],
        [8, 256, 16, 112, 112],
        [8, 256, 192, 28, 28],
        [8, 256, 32, 112, 112],
        [8, 256, 1280, 7, 7],
        [8, 256, 96, 112, 112],
        [8, 256, 144, 56, 56],
        [8, 256, 320, 7, 7],
        [8, 256, 960, 7, 7],
        [8, 256, 32, 28, 28],
        [8, 256, 576, 14, 14],
        [8, 256, 576, 7, 7],
    )
    for i in param_list:
        _test_fake_quant(i)

test_fake_quant_main()
