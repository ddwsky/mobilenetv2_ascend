import mindspore
import mindspore.context as context
import mindspore.nn as nn
from mindspore import Tensor, Parameter
from mindspore.common.initializer import initializer
from mindspore.ops import composite as C
from mindspore import Tensor
from mindspore.ops import operations as P
from mindspore.common.parameter import ParameterTuple
from mindspore.ops.op_info_register import op_info_register, TBERegOp, DataType

import os
import time
from functools import partial
import numpy as np
import torch

from op_impl.nn_quant import BatchNormFold


def init_weight(w, min=-1., max=1.):
    w.set_parameter_data(np.random.uniform(min, max, w.data.shape()).astype('float32'))


class Model(nn.Cell):
    def __init__(self, num_features, momentum, epsilon, freeze_bn, use_5HD):
        super(Model, self).__init__()
        self.use_5HD = use_5HD
        self.conv = nn.Conv2d(num_features, num_features, kernel_size=1)
        self.batchnorm_fold_train = BatchNormFold(momentum, epsilon, is_training=True, freeze_bn=freeze_bn)
        self.batchnorm_fold_infer = BatchNormFold(momentum, epsilon, is_training=False, freeze_bn=freeze_bn)

        self.moving_mean = Parameter(initializer('zeros', num_features), name="moving_mean", requires_grad=False)
        self.moving_variance = Parameter(initializer('ones', num_features), name="moving_variance", requires_grad=False)
        self.step = Parameter(initializer('zeros', [1], dtype=mindspore.int32), name='step', requires_grad=False)
        np.random.seed(0)
        init_weight(self.conv.weight, -10, 10)
        init_weight(self.moving_mean, -0.5, 1.2)
        init_weight(self.moving_variance, 0, 3)

    def construct(self, x):
        if self.use_5HD:
            x = self.conv(x)
        if self.training:
            return self.batchnorm_fold_train(x, self.moving_mean, self.moving_variance, self.step)
        else:
            return self.batchnorm_fold_infer(x, self.moving_mean, self.moving_variance, self.step)


def test_forward(data_foramt, is_train):
    assert data_foramt in (DataType.F32_NCHW, DataType.F32_5HD)
    assert isinstance(is_train, bool)

    num_features = 32
    momentum = 0.5
    epsilon = 1e-5
    freeze_bn = 1000
    use_5HD = data_foramt == DataType.F32_5HD
    net = Model(num_features, momentum, epsilon, freeze_bn, use_5HD)

    net.set_train(is_train)
    np.random.seed(0)
    for i in range(10):
        x = Tensor(np.random.uniform(-0.5, 1, size=[64, num_features, 224, 224]).astype('float32'))
        start = time.time()
        outs = net(x)
        print("time={:.3}s".format(time.time() - start))
    print("batch_mean", outs[0].asnumpy())
    print("batch_std", outs[1].asnumpy())
    print("running_mean", outs[2].asnumpy())
    print("running_std", outs[3].asnumpy())
    print("moving_mean", net.moving_mean.data.asnumpy())
    print("moving_variance", net.moving_variance.data.asnumpy())


def test_backward(data_foramt, get_input_grad):
    assert data_foramt in (DataType.F32_NCHW, DataType.F32_5HD)
    assert isinstance(get_input_grad, bool)

    num_features = 19
    momentum = 0.5
    epsilon = 1e-5
    freeze_bn = 1000
    use_5HD = data_foramt == DataType.F32_5HD
    net = Model(num_features, momentum, epsilon, freeze_bn, use_5HD)

    net.set_train()
    np.random.seed(0)
    for i in range(3):
        x = Tensor(np.random.uniform(-0.5, 1, size=[10, num_features, 10, 10]).astype('float32'))
        outs = net(x)
        if get_input_grad:
            grads = C.grad_all(net)(x)
        else:
            grads = C.grad_by_list(net, ParameterTuple(net.trainable_params()))(x)
        for grad in grads:
            print(grad.shape(), grad.asnumpy().flatten()[:3], grad.asnumpy().flatten()[-3:])


if __name__ == "__main__":
    if torch.cuda.device_count() == 0:
        os.system("rm -rf kernel_meta")
        os.system("rm -rf /opt/npu/w00447567/workspace/tbe/impl/__pycache__")
        context.set_context(mode=context.GRAPH_MODE, device_target='Ascend', device_id=5)
    else:
        context.set_context(mode=context.GRAPH_MODE, device_target='GPU', device_id=0)

    # test_forward(data_foramt=DataType.F32_NCHW, is_train=True)  # 0.250 0.433 0.251 0.434 0.250 0.188
    # test_forward(data_foramt=DataType.F32_NCHW, is_train=False)  # 0.    1.    0.823 0.965 0.823 0.931
    # test_forward(data_foramt=DataType.F32_5HD, is_train=True)  # 12.37 14.19 12.34 14.17 12.35 201.2
    # test_forward(data_foramt=DataType.F32_5HD, is_train=False)  # 0. 1. 0.823 0.965 0.823 9.318

    # test_backward(data_foramt=DataType.F32_5HD, get_input_grad=True)  # 0.02876282 0.04257202 0.0541687
    test_backward(data_foramt=DataType.F32_5HD, get_input_grad=False)  # 0.29077685 0.3192605  0.31703135
