# Copyright 2020 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================

import numpy as np
import pytest
from mindspore import Tensor
import mindspore.nn as nn
from mindspore.common.api import ms_function
from mindspore.ops import operations as P
import mindspore.context as context

context.set_context(device_target='Ascend', device_id=0)


class Net(nn.Cell):
    def __init__(self, num_bits=8):
        from op_impl import fake_quant_with_min_max_perchannel_grad
        from op_impl import primitive as PP

        super(Net, self).__init__()
        self.op = PP.FakeQuantPerChannelGradAscend(num_bits=8)

    @ms_function
    def construct(self, dout, x, min, max):
        return self.op(dout, x, min, max)


def numpy_result(dout, x, min_val, max_val, channel_axis, num_bits=8):
    quant_min = 0
    quant_max = 2 ** num_bits - 1
    scale = (max_val - min_val) / (quant_max - quant_min)
    zp_from_min = quant_min - (min_val / scale)
    # Nudge zero pointv
    nudge_zp = np.round(np.minimum(quant_max, np.maximum(quant_min, zp_from_min)))
    nudge_min_ = (quant_min - nudge_zp) * scale
    nudge_max_ = (quant_max - nudge_zp) * scale
    # boardcast
    nudge_min = np.reshape(nudge_min_, (x.shape[1], 1, 1))
    nudge_max = np.reshape(nudge_max_, (x.shape[1], 1, 1))

    dx = np.where(np.logical_and(x > nudge_min, x < nudge_max), dout, np.zeros_like(dout))
    return dx, nudge_min, nudge_max


def _test_fake_quant_grad(param):
    num_bits, *shapes = param
    dout = np.random.rand(*shapes).astype('float32')
    x = np.random.uniform(-1, 1, size=[*shapes]).astype('float32')
    min = np.random.uniform(-1, 0, size=[shapes[1]]).astype('float32')
    max = np.random.uniform(0, 1, size=[shapes[1]]).astype('float32')

    expect_dx, nudge_min, nudge_max = numpy_result(dout, x, min, max, 1)

    net = Net(num_bits=num_bits)
    output = net(Tensor(dout), Tensor(x), Tensor(min), Tensor(max))

    try:
        assert (np.allclose(output.asnumpy(), expect_dx, rtol=1.e-5, atol=1.e-5))
    except AssertionError:
        # print("=" * 20, "output", "=" * 20)
        # print(output.asnumpy())
        # print("=" * 20, "expect_dx", "=" * 20)
        # print(expect_dx)
        print("=" * 20, "diff", "=" * 20)
        diff = output.asnumpy() - expect_dx
        print(x[diff != 0])
        print("x shape:{}".format(x.shape))
        print("nudge_min:{}, \nnudge_max:{}".format(nudge_min.shape, nudge_max.shape))
        raise


def test_fake_quant_grad():
    # num_bits, *shapes
    param_list = (
        [8, 256, 24, 56, 56],
        [8, 256, 64, 14, 14],
        [8, 256, 160, 7, 7],
        [8, 256, 96, 56, 56],
        [8, 256, 96, 14, 14],
        [8, 256, 192, 14, 14],
        [8, 256, 384, 14, 14],
        [8, 256, 144, 28, 28],
        [8, 256, 16, 112, 112],
        [8, 256, 192, 28, 28],
        [8, 256, 32, 112, 112],
        [8, 256, 1280, 7, 7],
        [8, 256, 96, 112, 112],
        [8, 256, 144, 56, 56],
        [8, 256, 320, 7, 7],
        [8, 256, 960, 7, 7],
        [8, 256, 32, 28, 28],
        [8, 256, 576, 14, 14],
        [8, 256, 576, 7, 7],
    )
    for i in param_list:
        _test_fake_quant_grad(i)
