# Copyright 2020 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================

import numpy as np
import pytest
from mindspore import Tensor
from mindspore import nn
import mindspore.context as context
from mindspore.common import dtype as mstype
from mindspore.common.api import ms_function

context.set_context(device_target='Ascend', device_id=0)


def fake_quant_np(x, min_val, max_val, num_bits=8, symmetric=False, narrow_range=False, channel_axis=1):
    if symmetric:
        quant_min = 0 - 2 ** (num_bits - 1)
        quant_max = 2 ** (num_bits - 1) - 1
    else:
        quant_min = 0
        quant_max = 2 ** num_bits - 1
    if narrow_range:
        quant_min = quant_min + 1

    # CalNudge
    scale = (min_val - max_val)/(quant_max - quant_min)
    zp_from_min = quant_min - min_val/scale

    nudge_zp = np.floor(np.minimum(
        quant_max, np.maximum(quant_min, zp_from_min)) + 0.5)
    nudge_min = np.float32((quant_min - nudge_zp) * scale)
    nudge_max = np.float32((quant_max - nudge_zp) * scale)

    # boardcast
    print(x.shape)
    print(min_val.shape)
    if x.shape > min_val.shape:
        nudge_min = np.reshape(nudge_min, (x.shape[1], 1, 1))
        nudge_max = np.reshape(nudge_max, (x.shape[1], 1, 1))
        scale = np.reshape(scale, (x.shape[1], 1, 1))

    out = np.minimum(np.maximum(x, nudge_min), nudge_max)
    nudge_input = np.floor((out - nudge_min)/scale + 0.5)
    res = nudge_input * scale + nudge_min
    return [res, nudge_min, nudge_max]


class Net(nn.Cell):
    def __init__(self, num_bits=8, symmetric=False, narrow_range=False, channel_axis=1):
        from op_impl import fake_quant_with_min_max_perchannel
        from op_impl import primitive as PP

        super(Net, self).__init__()
        self.op = PP.FakeQuantPerChannelAscend(num_bits=num_bits,
                                               symmetric=symmetric,
                                               narrow_range=narrow_range,
                                               channel_axis=channel_axis)

    @ms_function
    def construct(self, x, min, max):
        return self.op(x, min, max)


def test_fake_quant_perchannel1():
    # WithVarsPerChannel_ZeroMinAndMax
    x = np.array([0.0, 0.0, 0.0, 0.0]).astype(np.float32)
    min_val = np.array([0.0, 0.0, 0.0, 0.0]).astype(np.float32)
    max_val = np.array([0.0, 0.0, 0.0, 0.0]).astype(np.float32)
    expect = np.array([0.0, 0.0, 0.0, 0.0]).astype(np.float32)

    net = Net(num_bits=8, narrow_range=False, channel_axis=0)
    output = net(Tensor(x), Tensor(min_val), Tensor(max_val))

    error = np.ones(shape=expect.shape) * 1.0e-5
    diff = output.asnumpy() - expect
    print("output: ", output)
    print("expect: ", expect)
    assert np.all(np.abs(diff) < error)


def test_fake_quant_perchannel2():
    # WithVarsPerChannelDim1NudgedDown_RegularRange
    # scale 1/4, zp 0.4, nudge 0. nudged ranges [0.0, 63.75]
    x = np.array([-0.1, 0.0, 63.75, 63.8]).astype(np.float32)
    min_val = np.array([-0.1, -0.1, -0.1, -0.1]).astype(np.float32)
    max_val = np.array([63.65, 63.65, 63.65, 63.65]).astype(np.float32)
    expect = np.array([0.0, 0.0, 63.75, 63.75]).astype(np.float32)

    net = Net(num_bits=8, narrow_range=False, channel_axis=0)
    output = net(Tensor(x), Tensor(min_val), Tensor(max_val))

    error = np.ones(shape=expect.shape) * 1.0e-5
    diff = output.asnumpy() - expect
    print("output: ", output)
    print("expect: ", expect)
    assert np.all(np.abs(diff) < error)


def test_fake_quant_perchannel3():
    # WithVarsPerChannelDim1NudgedDown_NarrowRange
    # scale 1/4, zp 1.4, nudge 1. nudged ranges[0.0, 63.5]
    x = np.array([-0.1, 0.0, 63.5, 63.6]).astype(np.float32)
    min_val = np.array([-0.1, -0.1, -0.1, -0.1]).astype(np.float32)
    max_val = np.array([63.4, 63.4, 63.4, 63.4]).astype(np.float32)
    expect = np.array([0.0, 0.0, 63.5, 63.5]).astype(np.float32)

    net = Net(num_bits=8, narrow_range=True, channel_axis=0)
    output = net(Tensor(x), Tensor(min_val), Tensor(max_val))

    error = np.ones(shape=expect.shape) * 1.0e-5
    diff = output.asnumpy() - expect
    print("output: ", output)
    print("expect: ", expect)
    assert np.all(np.abs(diff) < error)


def test_fake_quant_perchannel4():
    # WithVarsPerChannelDim1NudgedUp_RegularRange
    # [-0.125, 63.625]
    # scale 1/4, zp: 0.5, nudge 0. nudged range [-0.25, 63.5]
    x = np.array([-0.26, -0.25, -0.24, 63.6]).astype(np.float32)
    expect = np.array([-0.25, -0.25, -0.25, 63.5]).astype(np.float32)
    min_val = np.array([-0.125, -0.125, -0.125, -0.125]).astype(np.float32)
    max_val = np.array([63.625, 63.625, 63.625, 63.625]).astype(np.float32)

    net = Net(num_bits=8, narrow_range=False, channel_axis=0)
    output = net(Tensor(x), Tensor(min_val), Tensor(max_val))
    expect_np, _, _ = fake_quant_np(x, min_val, max_val, 8, False, False)

    error = np.ones(shape=expect.shape) * 1.0e-5
    diff = output.asnumpy() - expect
    print("output: ", output)
    print("expect: ", expect)
    print("expect np:", expect_np)
    assert np.all(np.abs(diff) < error)


def test_fake_quant_perchannel5():
    # WithVarsPerChannelDim1NudgedUp_NarrowRange
    # scale 1/4, zp: 1.5, nudge 2. nudged range [-0.25, 63.25]
    x = np.array([-0.26, -0.25, -0.24, 63.3]).astype(np.float32)
    expect = np.array([-0.25, -0.25, -0.25, 63.25]).astype(np.float32)
    min_val = np.array([-0.125, -0.125, -0.125, -0.125]).astype(np.float32)
    max_val = np.array([63.375, 63.375, 63.375, 63.375]).astype(np.float32)

    net = Net(num_bits=8, narrow_range=True, channel_axis=0)
    output = net(Tensor(x), Tensor(min_val), Tensor(max_val))
    expect_np, _, _ = fake_quant_np(x, min_val, max_val, 8, False, True)

    error = np.ones(shape=expect.shape) * 1.0e-5
    diff = output.asnumpy() - expect
    print("output: ", output)
    print("expect: ", expect)
    print("expect np:", expect_np)
    assert np.all(np.abs(diff) < error)


def test_fake_quant_perchannel6():
    # WithVarsPerChannelDim2NudgedDown_RegularRange
    # scale 1/4, zp: 0.4, nudge 0. nudged range [-0.25, 63.75]
    x = np.array([-0.1, 0.0, 0.1, 0.25, 63.75, 63.80]
                 ).reshape(2, 3).astype(np.float32)
    expect = np.array([-0.0, 0.0, 0.0, 0.25, 63.75, 63.75]).astype(np.float32)
    min_val = np.array([-0.1, -0.1, -0.1]).reshape(3).astype(np.float32)
    max_val = np.array([63.65, 63.65, 63.65]).reshape(3).astype(np.float32)

    net = Net(num_bits=8, narrow_range=False, channel_axis=1)
    output = net(Tensor(x), Tensor(min_val), Tensor(max_val))

    error = np.ones(shape=expect.shape) * 1.0e-5
    diff = output.asnumpy().flatten() - expect
    print("output: ", output)
    print("expect: ", expect)
    assert np.all(np.abs(diff) < error)


def test_fake_quant_perchannel7():
    # WithVarsPerChannelDim2NudgedDown_NarrowRange
    # scale 1/4, zp: 1.4, nudge 1. nudged range [-0.25, 63.5]
    x = np.array([-0.1, 0.0, 0.1, 0.25, 63.5, 63.6]
                 ).reshape(2, 3).astype(np.float32)
    expect = np.array([0.0, 0.0, 0.0, 0.25, 63.5, 63.5]).astype(np.float32)
    min_val = np.array([-0.1, -0.1, -0.1]).reshape(3).astype(np.float32)
    max_val = np.array([63.4, 63.4, 63.4]).reshape(3).astype(np.float32)

    net = Net(num_bits=8, narrow_range=True, channel_axis=1)
    output = net(Tensor(x), Tensor(min_val), Tensor(max_val))

    error = np.ones(shape=expect.shape) * 1.0e-5
    diff = output.asnumpy().flatten() - expect
    print("output: ", output)
    print("expect: ", expect)
    assert np.all(np.abs(diff) < error)


def test_fake_quant_perchannel8():
    # WithVarsPerChannelDim2NudgedUp_RegularRange
    # scale 1/4, zp: 0.5, nudge 1. nudged range [-0.25, 63.5]
    x = np.array([-0.26, -0.25, -0.24, 0.0, 63.5, 63.6]
                 ).reshape(2, 3).astype(np.float32)
    expect = np.array([-0.25, -0.25, -0.25, 0.0, 63.5, 63.5]
                      ).astype(np.float32)
    min_val = np.array([-0.125, -0.125, -0.125]).reshape(3).astype(np.float32)
    max_val = np.array([63.625, 63.625, 63.625]).reshape(3).astype(np.float32)

    net = Net(num_bits=8, narrow_range=False, channel_axis=1)
    output = net(Tensor(x), Tensor(min_val), Tensor(max_val))

    error = np.ones(shape=expect.shape) * 1.0e-5
    diff = output.asnumpy().flatten() - expect
    print("output: ", output)
    print("expect: ", expect)
    assert np.all(np.abs(diff) < error)


def test_fake_quant_perchannel9():
    # WithVarsPerChannelDim2NudgedUp_NarrowRange
    # scale 1/4, zp: 0.5, nudge 2. nudged range [-0.25, 63.25]
    x = np.array([-0.26, -0.25, -0.24, 0.0, 63.25, 63.3]
                 ).reshape(2, 3).astype(np.float32)
    expect = np.array(
        [-0.25, -0.25, -0.25, 0.0, 63.25, 63.25]).astype(np.float32)
    min_val = np.array([-0.125, -0.125, -0.125]).reshape(3).astype(np.float32)
    max_val = np.array([63.375, 63.375, 63.375]).reshape(3).astype(np.float32)

    net = Net(num_bits=8, narrow_range=True, channel_axis=1)
    output = net(Tensor(x), Tensor(min_val), Tensor(max_val))

    error = np.ones(shape=expect.shape) * 1.0e-5
    diff = output.asnumpy().flatten() - expect
    print("output: ", output)
    print("expect: ", expect)
    assert np.all(np.abs(diff) < error)


def test_fake_quant_perchannel10():
    # WithVarsPerChannelDim4NudgedDown_RegularRange
    # scale 1/4, zp: 0.4, nudge 0. nudged range [-0.25, 63.25]
    x = np.array([-0.1,   0.0,   0.1,   0.25,  0.5,    0.75,
                  1.0,   1.25,  1.5,   1.75,  2.0,    2.25,
                  63.0,  63.25, 63.5,  63.7,  63.75,  63.8,
                  63.9, 100.0, 100.0, 100.0, 100.0, 1000.0]).reshape(1, 4, 2, 3).astype(np.float32)
    expect = np.array([0.0,   0.0,   0.0,   0.25,  0.5,    0.75,
                       1.0,   1.25,  1.5,   1.75,  2.0,    2.25,
                       63.0,  63.25, 63.5,  63.75, 63.75,  63.75,
                       63.75, 63.75, 63.75, 63.75, 63.75,  63.75]).astype(np.float32)
    min_val = np.array([-0.1, -0.1, -0.1, -0.1]).reshape(4).astype(np.float32)
    max_val = np.array([63.65, 63.65, 63.65, 63.65]
                       ).reshape(4).astype(np.float32)

    net = Net(num_bits=8, narrow_range=False, channel_axis=1)
    output = net(Tensor(x), Tensor(min_val), Tensor(max_val))

    error = np.ones(shape=expect.shape) * 1.0e-5
    diff = output.asnumpy().flatten() - expect

    print("output: ", output)
    print("expect: ", expect)
    assert np.all(np.abs(diff) < error)


def test_fake_quant_perchannel11():
    # WithVarsPerChannelDim4NudgedDown_NarrowRange
    # scale 1/4, zp: 1.4, nudge 1. nudged range [0.0, 63.25]
    x = np.array([-0.1,   0.0,   0.1,   0.25,  0.5,    0.75,
                  1.0,   1.25,  1.5,   1.75,  2.0,    2.25,
                  63.0,  63.25, 63.3,  63.4,  63.5,   63.6,
                  63.7, 100.0, 100.0, 100.0, 100.0, 1000.0]).reshape(1, 4, 2, 3).astype(np.float32)
    expect = np.array([0.0,   0.0,   0.0,   0.25,  0.5,    0.75,
                       1.0,   1.25,  1.5,   1.75,  2.0,    2.25,
                       63.0,  63.25, 63.25, 63.5,  63.5,   63.5,
                       63.5,  63.5,  63.5,  63.5,  63.5,   63.5]).astype(np.float32)
    min_val = np.array([-0.1, -0.1, -0.1, -0.1]).reshape(4).astype(np.float32)
    max_val = np.array([63.4, 63.4, 63.4, 63.4]).reshape(4).astype(np.float32)

    net = Net(num_bits=8, narrow_range=True, channel_axis=1)
    output = net(Tensor(x), Tensor(min_val), Tensor(max_val))

    error = np.ones(shape=expect.shape) * 1.0e-5
    diff = output.asnumpy().flatten() - expect
    print("output: ", output)
    print("expect: ", expect)
    assert np.all(np.abs(diff) < error)


def test_fake_quant_perchannel12():
    #WithVarsPerChannelDim4NudgedUp_RegularRange
    # scale 1/4, zp: 0.5, nudge 2. nudged range [-0.25, 63.25]
    x = np.array([-0.3,  -0.25, -0.2,   0.0,    0.25,  0.5,
                  0.75,  1.0,   1.25,  1.5,    1.75,  2.0,
                  63.0,  63.25, 63.4,  63.5,   63.6,  63.7,
                  100.0, 100.0, 100.0, 100.0, 100.0, 1000.0]).reshape(1, 4, 2, 3).astype(np.float32)
    expect = np.array([-0.25,  -0.25, -0.25,  0.0,   0.25,   0.5,
                       0.75,   1.0,   1.25,  1.5,   1.75,   2.0,
                       63.0,  63.25, 63.5,  63.5,  63.5,   63.5,
                       63.5,  63.5,  63.5,  63.5,  63.5,   63.5]).astype(np.float32)
    min_val = np.array([-0.125, -0.125, -0.125, -0.125]
                       ).reshape(4).astype(np.float32)
    max_val = np.array([63.625, 63.625, 63.625, 63.625]
                       ).reshape(4).astype(np.float32)

    net = Net(num_bits=8, narrow_range=False, channel_axis=1)
    output = net(Tensor(x), Tensor(min_val), Tensor(max_val))

    error = np.ones(shape=expect.shape) * 1.0e-5
    diff = output.asnumpy().flatten() - expect
    print("output: ", output)
    print("expect: ", expect)
    assert np.all(np.abs(diff) < error)


def test_fake_quant_perchannel13():
    #WithVarsPerChannelDim4NudgedUp_NarrowRange
    # scale 1/4, zp: 0.5, nudge 2. nudged range [-0.25, 63.25]
    x = np.array([-0.3,  -0.25, -0.2,   0.0,   0.25,   0.5,
                  0.75,  1.0,   1.25,  1.5,   1.75,   2.0,
                  63.0,  63.2,  63.25, 63.3,  63.4,   63.5,
                  100.0, 100.0, 100.0, 100.0, 100.0, 1000.0]).reshape(1, 4, 2, 3).astype(np.float32)
    expect = np.array([-0.25, -0.25, -0.25,  0.0,   0.25,   0.5,
                       0.75,  1.0,   1.25,  1.5,   1.75,   2.0,
                       63.0,  63.25, 63.25, 63.25, 63.25,  63.25,
                       63.25, 63.25, 63.25, 63.25, 63.25,  63.25]).astype(np.float32)
    min_val = np.array([-0.125, -0.125, -0.125, -0.125]
                       ).reshape(4).astype(np.float32)
    max_val = np.array([63.375, 63.375, 63.375, 63.375]
                       ).reshape(4).astype(np.float32)

    net = Net(num_bits=8, narrow_range=True, channel_axis=1)
    output = net(Tensor(x), Tensor(min_val), Tensor(max_val))

    error = np.ones(shape=expect.shape) * 1.0e-5
    diff = output.asnumpy().flatten() - expect
    print("output: ", output)
    print("expect: ", expect)
    assert np.all(np.abs(diff) < error)


def test_fake_quant_perchannel14():
    #WithVarsPerChannelDim1NudgedDown_4Bits_RegularRange
    # scale 1/4, zp: 0.5, nudge 2. nudged range [-0.25, 63.25]
    x = np.array([-0.1, 0.0, 7.5, 7.6]).reshape(4).astype(np.float32)
    expect = np.array([0.0, 0.0, 7.5, 7.5]).astype(np.float32)
    min_val = np.array([-0.1, -0.1, -0.1, -0.1]).reshape(4).astype(np.float32)
    max_val = np.array([7.4, 7.4, 7.4, 7.4]).reshape(4).astype(np.float32)

    net = Net(num_bits=4, narrow_range=False, channel_axis=0)
    output = net(Tensor(x), Tensor(min_val), Tensor(max_val))
    expect_np, _, _ = fake_quant_np(x, min_val, max_val, 4, False, False)

    error = np.ones(shape=expect.shape) * 1.0e-5
    diff = output.asnumpy().flatten() - expect
    print("output: ", output)
    print("expect: ", expect)
    print("expect_np: ", expect_np)
    assert np.all(np.abs(diff) < error)


def test_fake_quant_perchannel15():
    # WithVarsPerChannelDim1NudgedDown_4Bits_NarrowRange
    # scale 1/4, zp: 0.5, nudge 2. nudged range [-0.25, 63.25]
    x = np.array([-0.1, 0.0, 7.0, 7.1]).reshape(4).astype(np.float32)
    expect = np.array([0.0, 0.0, 7.0, 7.0]).astype(np.float32)
    min_val = np.array([-0.1, -0.1, -0.1, -0.1]).reshape(4).astype(np.float32)
    max_val = np.array([6.9, 6.9, 6.9, 6.9]).reshape(4).astype(np.float32)

    net = Net(num_bits=4, narrow_range=True, channel_axis=0)
    output = net(Tensor(x), Tensor(min_val), Tensor(max_val))

    error = np.ones(shape=expect.shape) * 1.0e-5
    diff = output.asnumpy() - expect
    print("output: ", output)
    print("expect: ", expect)
    assert np.all(np.abs(diff) < error)


def test_fake_quant_perchannel16():
    # WithVarsPerChannelDim1NudgedUp_4Bits_RegularRange
    # scale 1/4, zp: 0.5, nudge 2. nudged range [-0.25, 63.25]
    x = np.array([-0.6, -0.5, 7.0, 7.1]).reshape(4).astype(np.float32)
    expect = np.array([-0.5, -0.5, 7.0, 7.0]).astype(np.float32)
    min_val = np.array([-0.4, -0.4, -0.4, -0.4]).reshape(4).astype(np.float32)
    max_val = np.array([7.1, 7.1, 7.1, 7.1]).reshape(4).astype(np.float32)

    net = Net(num_bits=4, narrow_range=False, channel_axis=0)
    output = net(Tensor(x), Tensor(min_val), Tensor(max_val))

    error = np.ones(shape=expect.shape) * 1.0e-5
    diff = output.asnumpy() - expect
    print("output: ", output)
    print("expect: ", expect)
    assert np.all(np.abs(diff) < error)


def test_fake_quant_perchannel17():
    # WithVarsPerChannelDim1NudgedUp_4Bits_NarrowRange
    # scale 1/4, zp: 0.5, nudge 2. nudged range [-0.25, 63.25]
    x = np.array([-0.6, -0.5, 6.5, 6.6]).reshape(4).astype(np.float32)
    expect = np.array([-0.5, -0.5, 6.5, 6.5]).astype(np.float32)
    min_val = np.array([-0.4, -0.4, -0.4, -0.4]).reshape(4).astype(np.float32)
    max_val = np.array([6.6, 6.6, 6.6, 6.6]).reshape(4).astype(np.float32)

    net = Net(num_bits=4, narrow_range=True, channel_axis=0)
    output = net(Tensor(x), Tensor(min_val), Tensor(max_val))

    error = np.ones(shape=expect.shape) * 1.0e-5
    diff = output.asnumpy() - expect
    print("output: ", output)
    print("expect: ", expect)
    assert np.all(np.abs(diff) < error)


def test_fake_quant_perchannel18():
    # WithVarsPerChannelDim2NudgedDown_4Bits_RegularRange
    # scale 1/4, zp: 0.5, nudge 2. nudged range [-0.25, 63.25]
    x = np.array([-0.1, 0.0, 0.1, 0.5, 7.5, 7.6]
                 ).reshape(2, 3).astype(np.float32)
    expect = np.array([0.0, 0.0, 0.0, 0.5, 7.5, 7.5]).astype(np.float32)
    min_val = np.array([-0.1, -0.1, -0.1]).reshape(3).astype(np.float32)
    max_val = np.array([7.4, 7.4, 7.4]).reshape(3).astype(np.float32)

    net = Net(num_bits=4, narrow_range=False, channel_axis=1)
    output = net(Tensor(x), Tensor(min_val), Tensor(max_val))

    error = np.ones(shape=expect.shape) * 1.0e-5
    diff = output.asnumpy().flatten() - expect
    print("output: ", output)
    print("expect: ", expect)
    assert np.all(np.abs(diff) < error)


def test_fake_quant_perchannel19():
    # WithVarsPerChannelDim2NudgedDown_4Bits_NarrowRange
    # scale 1/4, zp: 0.5, nudge 2. nudged range [-0.25, 63.25]
    x = np.array([-0.1, 0.0, 0.1, 0.5, 7.0, 7.1]
                 ).reshape(2, 3).astype(np.float32)
    expect = np.array([0.0, 0.0, 0.0, 0.5, 7.0, 7.0]).astype(np.float32)
    min_val = np.array([-0.1, -0.1, -0.1]).reshape(3).astype(np.float32)
    max_val = np.array([6.9, 6.9, 6.9]).reshape(3).astype(np.float32)

    net = Net(num_bits=4, narrow_range=True, channel_axis=1)
    output = net(Tensor(x), Tensor(min_val), Tensor(max_val))

    error = np.ones(shape=expect.shape) * 1.0e-5
    diff = output.asnumpy().flatten() - expect
    print("output: ", output)
    print("expect: ", expect)
    assert np.all(np.abs(diff) < error)


def test_fake_quant_perchannel20():
    # WithVarsPerChannelDim2NudgedUp_4Bits_RegularRange
    # scale 1/4, zp: 0.5, nudge 2. nudged range [-0.25, 63.25]
    x = np.array([-0.51, -0.5, -0.24, 0.0, 7.0, 7.1]
                 ).reshape(2, 3).astype(np.float32)
    expect = np.array([-0.5, -0.5, 0.0, 0.0, 7.0, 7.0]).astype(np.float32)
    min_val = np.array([-0.4, -0.4, -0.4]).reshape(3).astype(np.float32)
    max_val = np.array([7.1, 7.1, 7.1]).reshape(3).astype(np.float32)

    net = Net(num_bits=4, narrow_range=False, channel_axis=1)
    output = net(Tensor(x), Tensor(min_val), Tensor(max_val))

    error = np.ones(shape=expect.shape) * 1.0e-5
    diff = output.asnumpy().flatten() - expect
    print("output: ", output)
    print("expect: ", expect)
    assert np.all(np.abs(diff) < error)


def test_fake_quant_perchannel21():
    # WithVarsPerChannelDim2NudgedUp_4Bits_NarrowRange
    # scale 1/4, zp: 0.5, nudge 2. nudged range [-0.25, 63.25]
    x = np.array([-0.6, -0.5, -0.24, 0.0, 6.5, 6.6]
                 ).reshape(2, 3).astype(np.float32)
    expect = np.array([-0.5, -0.5, 0.0, 0.0, 6.5, 6.5]).astype(np.float32)
    min_val = np.array([-0.4, -0.4, -0.4]).reshape(3).astype(np.float32)
    max_val = np.array([6.6, 6.6, 6.6]).reshape(3).astype(np.float32)

    net = Net(num_bits=4, narrow_range=True, channel_axis=1)
    output = net(Tensor(x), Tensor(min_val), Tensor(max_val))

    error = np.ones(shape=expect.shape) * 1.0e-5
    diff = output.asnumpy().flatten() - expect
    print("output: ", output)
    print("expect: ", expect)
    assert np.all(np.abs(diff) < error)


def test_fake_quant_perchannel22():
    # WithVarsPerChannelDim4NudgedDown_4Bits_RegularRange
    # scale 1/4, zp: 0.5, nudge 2. nudged range [-0.25, 63.25]
    x = np.array([-0.1,   0.0,   0.1,   0.5,   1.0,    1.5,
                  1.5,   2.0,   2.5,   3.0,   3.5,    4.0,
                  6.0,   6.5,   7.0,   7.4,   7.5,    7.7,
                  7.8, 100.0, 100.0, 100.0, 100.0, 1000.0]).reshape(1, 4, 2, 3).astype(np.float32)
    expect = np.array([0.0,   0.0,   0.0,   0.5,   1.0,    1.5,
                       1.5,   2.0,   2.5,   3.0,   3.5,    4.0,
                       6.0,   6.5,   7.0,   7.5,   7.5,    7.5,
                       7.5,   7.5,   7.5,   7.5,   7.5,    7.5]).astype(np.float32)
    min_val = np.array([-0.1, -0.1, -0.1, -0.1]).reshape(4).astype(np.float32)
    max_val = np.array([7.4, 7.4, 7.4, 7.4]).reshape(4).astype(np.float32)

    net = Net(num_bits=4, narrow_range=False, channel_axis=1)
    output = net(Tensor(x), Tensor(min_val), Tensor(max_val))

    error = np.ones(shape=expect.shape) * 1.0e-5
    diff = output.asnumpy().flatten() - expect
    print("output: ", output)
    print("expect: ", expect)
    assert np.all(np.abs(diff) < error)


def test_fake_quant_perchannel23():
    # WithVarsPerChannelDim4NudgedDown_4Bits_NarrowRange
    # scale 1/4, zp: 0.5, nudge 2. nudged range [-0.25, 63.25]
    x = np.array([-0.1,   0.0,   0.1,   0.5,   1.0,    1.5,
                  1.5,   2.0,   2.5,   3.0,   3.5,    4.0,
                  6.0,   6.5,   6.8,   6.9,   7.0,    7.1,
                  7.2, 100.0, 100.0, 100.0, 100.0, 1000.0]).reshape(1, 4, 2, 3).astype(np.float32)
    expect = np.array([0.0,   0.0,   0.0,   0.5,   1.0,    1.5,
                       1.5,   2.0,   2.5,   3.0,   3.5,    4.0,
                       6.0,   6.5,   7.0,   7.0,   7.0,    7.0,
                       7.0,   7.0,   7.0,   7.0,   7.0,    7.0]).astype(np.float32)
    min_val = np.array([-0.1, -0.1, -0.1, -0.1]).reshape(4).astype(np.float32)
    max_val = np.array([6.9, 6.9, 6.9, 6.9]).reshape(4).astype(np.float32)

    net = Net(num_bits=4, narrow_range=True, channel_axis=1)
    output = net(Tensor(x), Tensor(min_val), Tensor(max_val))

    error = np.ones(shape=expect.shape) * 1.0e-5
    diff = output.asnumpy().flatten() - expect
    print("output: ", output)
    print("expect: ", expect)
    assert np.all(np.abs(diff) < error)


def test_fake_quant_perchannel24():
    # WithVarsPerChannelDim4NudgedUp_4Bits_RegularRange
    # scale 1/4, zp: 0.5, nudge 2. nudged range [-0.25, 63.25]
    x = np.array([-0.6,  -0.5,  -0.4,   0.0,   0.5,    1.0,
                  1.5,   2.0,   2.5,   3.0,   3.5,    4.0,
                  6.0,   6.5,   6.9,   7.0,   7.1,    7.7,
                  100.0, 100.0, 100.0, 100.0, 100.0, 1000.0]).reshape(1, 4, 2, 3).astype(np.float32)
    expect = np.array([-0.5, -0.5,   -0.5,   0.0,   0.5,    1.0,
                       1.5,  2.0,    2.5,   3.0,   3.5,    4.0,
                       6.0,  6.5,    7.0,   7.0,   7.0,    7.0,
                       7.0,  7.0,    7.0,   7.0,   7.0,    7.0]).astype(np.float32)
    min_val = np.array([-0.4, -0.4, -0.4, -0.4]).reshape(4).astype(np.float32)
    max_val = np.array([7.1, 7.1, 7.1, 7.1]).reshape(4).astype(np.float32)

    net = Net(num_bits=4, narrow_range=False, channel_axis=1)
    output = net(Tensor(x), Tensor(min_val), Tensor(max_val))

    error = np.ones(shape=expect.shape) * 1.0e-5
    diff = output.asnumpy().flatten() - expect
    print("output: ", output)
    print("expect: ", expect)
    assert np.all(np.abs(diff) < error)


def test_fake_quant_perchannel25():
    # WithVarsPerChannelDim4NudgedUp_4Bits_NarrowRange
    # scale 1/4, zp: 0.5, nudge 2. nudged range [-0.25, 63.25]
    x = np.array([-0.6,  -0.5,  -0.4,   0.0,   0.5,    1.0,
                  1.5,   2.0,   2.5,   3.0,   3.5,    4.0,
                  5.5,   6.0,   6.4,   6.5,   6.6,    6.7,
                  100.0, 100.0, 100.0, 100.0, 100.0, 1000.0]).reshape(1, 4, 2, 3).astype(np.float32)
    expect = np.array([-0.5, -0.5,  -0.5,   0.0,   0.5,    1.0,
                       1.5,   2.0,   2.5,   3.0,   3.5,    4.0,
                       5.5,   6.0,   6.5,   6.5,   6.5,    6.5,
                       6.5,   6.5,   6.5,   6.5,   6.5,    6.5]).astype(np.float32)
    min_val = np.array([-0.4, -0.4, -0.4, -0.4]).reshape(4).astype(np.float32)
    max_val = np.array([6.6, 6.6, 6.6, 6.6]).reshape(4).astype(np.float32)

    net = Net(num_bits=4, narrow_range=True, channel_axis=1)
    output = net(Tensor(x), Tensor(min_val), Tensor(max_val))

    error = np.ones(shape=expect.shape) * 1.0e-5
    diff = output.asnumpy().flatten() - expect
    print("output: ", output)
    print("expect: ", expect)
    assert np.all(np.abs(diff) < error)


def _test_fake_quant(param):
    num_bits, *shapes = param
    x = np.random.uniform(-7, 7, size=[*shapes]).astype('float32')
    min_val = np.random.uniform(-6, 0,
                                size=[shapes[1]]).reshape(shapes[1]).astype('float32')
    max_val = np.random.uniform(0, 6, size=[shapes[1]]).reshape(
        shapes[1]).astype('float32')

    net = Net(num_bits=8, narrow_range=True, channel_axis=1)
    output = net(Tensor(x), Tensor(min_val), Tensor(max_val))
    expect, min_np, max_np = fake_quant_np(
        x, min_val, max_val, num_bits=8, symmetric=False, narrow_range=False, channel_axis=1)

    error = np.ones(shape=expect.shape) * 1.0e-5
    diff = output.asnumpy() - expect
    print("output: ", output)
    print("expect: ", expect)
    assert np.all(np.abs(diff) < error)


# def test_fake_quant_perchannel26():
#     # num_bits, *shapes
#     param_list = (
#         [8, 256, 24, 56, 56],
#         [8, 256, 64, 14, 14],
#         [8, 256, 160, 7, 7],
#         [8, 256, 96, 56, 56],
#         [8, 256, 96, 14, 14],
#         [8, 256, 192, 14, 14],
#         [8, 256, 384, 14, 14],
#         [8, 256, 144, 28, 28],
#         [8, 256, 16, 112, 112],
#         [8, 256, 192, 28, 28],
#         [8, 256, 32, 112, 112],
#         [8, 256, 1280, 7, 7],
#         [8, 256, 96, 112, 112],
#         [8, 256, 144, 56, 56],
#         [8, 256, 320, 7, 7],
#         [8, 256, 960, 7, 7],
#         [8, 256, 32, 28, 28],
#         [8, 256, 576, 14, 14],
#         [8, 256, 576, 7, 7],
#     )
#     for i in param_list:
#         print("testing " + "=" * 20)
#         _test_fake_quant(i)
