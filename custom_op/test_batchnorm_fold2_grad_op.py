# Copyright 2020 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================

import numpy as np
import pytest
from mindspore import Tensor
from op_impl import primitive as P
import mindspore.nn as nn
from mindspore.common.api import ms_function
import mindspore.context as context

context.set_context(device_target='Ascend', device_id=0)


class Net(nn.Cell):
    def __init__(self):
        from op_impl import batchnorm_fold2_grad

        super(Net, self).__init__()
        self.op = P._BatchNormFold2Grad()

    @ms_function
    def construct(self, dout, x, gamma, batch_std, batch_mean, running_std):
        return self.op(dout, x, gamma, batch_std, batch_mean, running_std)


def _test_batchnrom_fold2_grad(shape):
    net = Net()
    n, c, h, w = shape
    dout = np.random.uniform(-1, 1, size=[n, c, h, w]).astype('float32')
    x = np.random.uniform(-1, 1, size=[n, c, h, w]).astype('float32')
    gamma = np.random.uniform(1, 2, size=[c]).astype('float32')
    batch_std = np.random.uniform(1, 2, size=[c]).astype('float32')
    batch_mean = np.random.uniform(1, 2, size=[c]).astype('float32')
    running_std = np.random.uniform(1, 2, size=[c]).astype('float32')
    outputs = net(Tensor(dout), Tensor(x), Tensor(gamma), Tensor(batch_std), Tensor(batch_mean), Tensor(running_std))

    expect = []
    reduce_dout = np.sum(dout, (0, 2, 3))
    reduce_dout_x = np.sum(dout * x, (0, 2, 3))
    # 'd_batch_std', 'd_batch_mean', 'd_beta', 'd_gamma', 'dx'
    expect.append((reduce_dout * gamma * batch_mean - reduce_dout_x * running_std) / (batch_std * batch_std))
    expect.append(-reduce_dout * gamma / batch_std)
    expect.append(reduce_dout)
    expect.append(-reduce_dout * batch_mean / batch_std)
    expect.append(dout * (running_std / batch_std).reshape(1, -1, 1, 1))
    for i, v in enumerate(outputs):
        try:
            assert (np.allclose(outputs[i].asnumpy(), expect[i], rtol=1.e-4, atol=1.e-4))
        except AssertionError:
            print("=" * 20, "output[%d]" % i, "=" * 20)
            print(outputs[i].asnumpy())
            print("=" * 20, "expect[%d]" % i, "=" * 20)
            print(expect[i])
            print("=" * 20, "diff[%d]" % i, "=" * 20)
            print(outputs[i].asnumpy() - expect[i])
            raise


def test_batchnrom_fold2_grad():
    shapes = (
        (64, 180, 7, 7), (80, 112, 14, 14), (80, 120, 28, 28), (80, 160, 7, 7), (80, 16, 112, 112), (80, 184, 14, 14),
        (80, 200, 14, 14), (80, 240, 14, 14), (80, 240, 28, 28), (80, 24, 56, 56), (80, 40, 28, 28), (80, 480, 14, 14),
        (80, 64, 112, 112), (80, 64, 56, 56), (80, 672, 14, 14), (80, 672, 7, 7), (80, 72, 28, 28), (80, 72, 56, 56),
        (80, 80, 14, 14), (80, 960, 7, 7)
    )
    for i in shapes:
        _test_batchnrom_fold2_grad(i)
