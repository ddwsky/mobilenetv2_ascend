# Copyright 2020 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================

import numpy as np
import pytest
from mindspore import Tensor
import mindspore.nn as nn
from mindspore.common.api import ms_function
from mindspore.ops import operations as P
import mindspore.context as context

context.set_context(device_target='Ascend', device_id=0)


class Net(nn.Cell):
    def __init__(self, ema=True, ema_decay=0.999):
        from op_impl import fake_quant_with_min_max_perchannel_update
        from op_impl import primitive as PP

        super(Net, self).__init__()
        self.op = PP.FakeQuantPerChannelUpdateAscend(num_bits=8,
                                                     ema=ema,
                                                     ema_decay=ema_decay,
                                                     symmetric=False,
                                                     narrow_range=False,
                                                     training=True,
                                                     channel_axis=1)

    @ms_function
    def construct(self, x, min, max):
        return self.op(x, min, max)


def numpy_result(x, min_val, max_val, ema, ema_decay):
    input_min = np.min(x, axis=(0, 2, 3))
    input_max = np.max(x, axis=(0, 2, 3))
    if ema:
        input_min = ema_decay * min_val + (1 - ema_decay) * input_min
        input_min = np.minimum(input_min, 0)
        input_max = ema_decay * max_val + (1 - ema_decay) * input_max
        input_max = np.maximum(input_max, 0)
    return input_min, input_max


def _test_fake_quant_update(param):
    ema, ema_decay, num_bits, *shapes = param
    x = np.random.uniform(-1, 1, size=[*shapes]).astype('float32')
    min = np.random.uniform(-1, 0, size=[shapes[1]]).astype('float32')
    max = np.random.uniform(0, 1, size=[shapes[1]]).astype('float32')

    np_min, np_max = numpy_result(x, min, max, ema, ema_decay)

    net = Net(ema, ema_decay)
    min_update, max_update = net(Tensor(x), Tensor(min), Tensor(max))

    assert (np.allclose(min_update.asnumpy(), np_min, rtol=1.e-5, atol=1.e-5))
    assert (np.allclose(max_update.asnumpy(), np_max, rtol=1.e-5, atol=1.e-5))


def test_fake_quant_grad():
    # num_bits, *shapes
    param_list = (
        [8, 256, 24, 56, 56],
        # [8, 256, 64, 14, 14],
        # [8, 256, 160, 7, 7],
        # [8, 256, 96, 56, 56],
        # [8, 256, 96, 14, 14],
        # [8, 256, 192, 14, 14],
        # [8, 256, 384, 14, 14],
        # [8, 256, 144, 28, 28],
        # [8, 256, 16, 112, 112],
        # [8, 256, 192, 28, 28],
        # [8, 256, 32, 112, 112],
        # [8, 256, 1280, 7, 7],
        # [8, 256, 96, 112, 112],
        # [8, 256, 144, 56, 56],
        # [8, 256, 320, 7, 7],
        # [8, 256, 960, 7, 7],
        # [8, 256, 32, 28, 28],
        # [8, 256, 576, 14, 14],
        # [8, 256, 576, 7, 7],
    )
    ema, ema_decay = True, 0.999
    for i in param_list:
        _test_fake_quant_update([ema, ema_decay] + i)
