import numpy as np
import mindspore.context as context
from mindspore.ops import operations as P
from mindspore.ops import functional as F
from mindspore.common.parameter import Parameter
from mindspore.common.tensor import Tensor
from mindspore.common import dtype as mstype
from mindspore.nn.cell import Cell
from mindspore.common.initializer import initializer

# import TBE register
from . import batchnorm_fold, batchnorm_fold_grad
from . import batchnorm_fold2, batchnorm_fold2_grad
from . import correction_mul, correction_mul_grad
from . import fake_quant_with_min_max, fake_quant_with_min_max_grad, fake_quant_with_min_max_update

# import Primitive
from .primitive import _BatchNormFold, BNTrainingReduce
from .primitive import _BatchNormFold2
from .primitive import FakeQuantWithMinMaxUpdate4D, FakeQuantWithMinMaxUpdate5D


class BatchNormFold(Cell):
    """
    Batch normalization folded.

    Args:
        momentum (float): Momentum value should be [0, 1]. Default: 0.1.
        epsilon (float): A small float number to avoid dividing by 0. 1e-5 if dtype in
            float32 else 1e-3. Default: 1e-5.
        is_training (bool): In training mode set True, else set False. Default: True.
        freeze_bn (int): Delay in steps at which computation switches from regular batch
            norm to frozen mean and std. Default: 0.

    Inputs:
        - **x** (Tensor) - Tensor of shape :math:`(N, C)`.
        - **mean** (Tensor) - Tensor of shape :math:`(C,)`.
        - **variance** (Tensor) - Tensor of shape :math:`(C,)`.
        - **global_step** (Tensor) - Tensor to record current global step.

    Outputs:
        Tuple of 4 Tensor, the normalized input and the updated parameters.

        - **batch_mean** (Tensor) - Tensor of shape :math:`(C,)`.
        - **batch_std** (Tensor) - Tensor of shape :math:`(C,)`.
        - **running_mean** (Tensor) - Tensor of shape :math:`(C,)`.
        - **running_std** (Tensor) - Tensor of shape :math:`(C,)`.

    """

    def __init__(self, momentum=0.9, epsilon=1e-5, freeze_bn=0):
        """init batch norm fold layer"""
        super(BatchNormFold, self).__init__()
        self.epsilon = epsilon
        self.is_gpu = context.get_context('device_target') == "GPU"
        if self.is_gpu:
            self.bn_train = P.BatchNormFold(momentum, epsilon, is_training=True, freeze_bn=freeze_bn)
            self.bn_infer = P.BatchNormFold(momentum, epsilon, is_training=False, freeze_bn=freeze_bn)
        else:
            self.bn_reduce = BNTrainingReduce()
            self.bn_update = _BatchNormFold(momentum, epsilon, is_training=True, freeze_bn=freeze_bn)

    def construct(self, x, mean, variance, global_step):
        if self.is_gpu:
            if self.training:
                return self.bn_train(x, mean, variance, global_step)
            else:
                return self.bn_infer(x, mean, variance, global_step)
        else:
            if self.training:
                x_sum, x_square_sum = self.bn_reduce(x)
                y, batch_mean, batch_std, running_mean, running_std, mean_updated, variance_updated = \
                    self.bn_update(x, x_sum, x_square_sum, mean, variance)
                P.Assign()(mean, mean_updated)
                P.Assign()(variance, variance_updated)
            else:
                batch_mean = P.ZerosLike()(variance)
                batch_std = P.OnesLike()(variance)
                running_mean = P.TensorAdd()(mean, 0.)
                running_std = P.Sqrt()(P.TensorAdd()(variance, self.epsilon))
            return batch_mean, batch_std, running_mean, running_std


class Conv2dBatchNormQuant(Cell):
    r"""
    2D convolution with BatchNormal op folded layer.

    For a more Detailed overview of Conv2d op.

    Args:
        in_channels (int): The number of input channel :math:`C_{in}`.
        out_channels (int): The number of output channel :math:`C_{out}`.
        kernel_size (Union[int, tuple]): Specifies the height and width of the 2D convolution window.
        stride (int): Specifies stride for all spatial dimensions with the same value.
        pad_mode: (str): Specifies padding mode. The optional values are "same", "valid", "pad". Default: "same".
        padding: (int): Implicit paddings on both sides of the input. Default: 0.
        eps (int): Parameters for BatchNormal. Default: 1e-5.
        momentum (int): Parameters for BatchNormal op. Default: 0.9.
        weight_init (Union[Tensor, str, Initializer, numbers.Number]): Initializer for the
            convolution kernel. Default: 'None'.
        beta_init (Union[Tensor, str, Initializer, numbers.Number]): Initializer for the
            beta vector. Default: 'None'.
        gamma_init (Union[Tensor, str, Initializer, numbers.Number]): Initializer for the
            gamma vector. Default: 'None'.
        mean_init (Union[Tensor, str, Initializer, numbers.Number]): Initializer for the
            mean vector. Default: 'None'.
        var_init (Union[Tensor, str, Initializer, numbers.Number]): Initializer for the
            variance vector. Default: 'None'.
        quant_delay (int): Quantization delay parameters according by global step. Default: 0.
        freeze_bn (int): Quantization freeze BatchNormal op according by global step. Default: 100000.
        fake (bool): Conv2dBatchNormQuant Cell add FakeQuantWithMinMax op or not. Default: True.
        num_bits (int): Quantization number bit, support 4 and 8bit. Default: 8.
        per_channel (bool): FakeQuantWithMinMax Parameters. Default: False.
        symmetric (bool): Quantization algorithm use symmetric or not. Default: False.
        narrow_range (bool): Quantization algorithm use narrow range or not. Default: False.

    Inputs:
        - **x** (Tensor) - Tensor of shape :math:`(N, C_{in}, H_{in}, W_{in})`.

    Outputs:
        Tensor of shape :math:`(N, C_{out}, H_{out}, W_{out})`.
    """

    def __init__(self,
                 in_channels,
                 out_channels,
                 kernel_size,
                 stride,
                 pad_mode,
                 padding=0,
                 dilation=1,
                 group=1,
                 eps=1e-5,
                 momentum=0.9,
                 weight_init=None,
                 beta_init=None,
                 gamma_init=None,
                 mean_init=None,
                 var_init=None,
                 quant_delay=0,
                 freeze_bn=100000,
                 fake=True,
                 num_bits=8,
                 per_channel=False,
                 symmetric=False,
                 narrow_range=False):
        """init Conv2dBatchNormQuant layer"""
        super(Conv2dBatchNormQuant, self).__init__()
        _ = dilation
        self.stride = stride
        if group > 1:
            assert group == in_channels
            assert group == out_channels
        self.is_depthwise = group > 1

        if self.is_depthwise:
            channel_multiplier = out_channels // in_channels
            self.conv = P.DepthwiseConv2dNative(channel_multiplier=channel_multiplier, kernel_size=kernel_size,
                                                stride=stride, pad_mode=pad_mode, pad=padding)
        else:
            self.conv = P.Conv2D(out_channel=out_channels,
                                 kernel_size=kernel_size,
                                 mode=1,
                                 pad_mode=pad_mode,
                                 pad=padding,
                                 stride=stride,
                                 dilation=1,
                                 group=group)
        self.fake = fake
        self.freeze_bn = freeze_bn
        if isinstance(kernel_size, int):
            kernel_size = (kernel_size, kernel_size)

        if weight_init is None:
            if self.is_depthwise:
                weight_init = initializer('normal', [channel_multiplier, in_channels, *kernel_size])
            else:
                weight_init = initializer('normal', [out_channels, in_channels, *kernel_size])
        self.weight = Parameter(weight_init, name='weight')
        if gamma_init is None:
            gamma_init = initializer('ones', [out_channels])
        self.gamma = Parameter(gamma_init, name='gamma')
        if beta_init is None:
            beta_init = initializer('zeros', [out_channels])
        self.beta = Parameter(beta_init, name='beta')
        if mean_init is None:
            mean_init = initializer('zeros', [out_channels])
        self.moving_mean = Parameter(
            mean_init, name='moving_mean', requires_grad=False)
        if var_init is None:
            var_init = initializer('ones', [out_channels])
        self.moving_variance = Parameter(
            var_init, name='moving_variance', requires_grad=False)

        self.step = Parameter(initializer(
            'normal', [1], dtype=mstype.int32), name='step', requires_grad=False)

        self.fake_quant_weight = FakeQuantWithMinMax(in_channels,
                                                     min_init=-6,
                                                     max_init=6,
                                                     ema=False,
                                                     num_bits=num_bits,
                                                     quant_delay=quant_delay,
                                                     per_channel=per_channel,
                                                     channel_size=out_channels,
                                                     symmetric=symmetric,
                                                     narrow_range=narrow_range)

        self.batchnorm_fold = BatchNormFold(epsilon=eps, momentum=momentum, freeze_bn=freeze_bn)
        self.correct_mul = P.CorrectionMul(1 if self.is_depthwise else 0)
        self.batchnorm_fold2_train = _BatchNormFold2(freeze_bn=freeze_bn)
        self.batchnorm_fold2_infer = _BatchNormFold2(freeze_bn=0)
        self.one = Tensor(1, mstype.int32)
        self.assignadd = P.AssignAdd()

    def extend_repr(self):
        s = 'fake={}, freeze_bn={}'.format(self.fake, self.freeze_bn)
        return s

    def construct(self, x):
        out_conv = self.conv(x, self.weight)

        # BN fold1
        batch_mean, batch_std, running_mean, running_std = self.batchnorm_fold(out_conv,
                                                                               self.moving_mean,
                                                                               self.moving_variance,
                                                                               self.step)

        # fake weight
        weight = self.correct_mul(self.weight, self.gamma, running_std)
        if self.fake:
            weight = self.fake_quant_weight(weight)
        out = self.conv(x, weight)

        # BN fold2
        if self.training:
            out = self.batchnorm_fold2_train(out, self.beta, self.gamma, batch_std, batch_mean, running_std)
            F.control_depend(out, self.assignadd(self.step, self.one))
        else:
            out = self.batchnorm_fold2_infer(out, self.beta, self.gamma, running_std, running_mean, running_std)
        return out


class FakeQuantWithMinMaxD(Cell):
    def __init__(self,
                 in_channel,
                 min_init=-6,
                 max_init=6,
                 num_bits=8,
                 ema=False,
                 ema_decay=0.999,
                 per_channel=False,
                 channel_size=1,
                 quant_delay=0,
                 symmetric=False,
                 narrow_range=False,
                 training=True):
        """init FakeQuantWithMinMax ascend layer"""
        super(FakeQuantWithMinMaxD, self).__init__()

        self.min_init = min_init
        self.num_bits = num_bits
        self.max_init = max_init
        self.ema = ema
        self.ema_decay = ema_decay
        self.per_channel = per_channel
        self.channel_size = channel_size
        self.quant_delay = quant_delay
        self.symmetric = symmetric
        self.narrow_range = narrow_range
        self.training = training

        if per_channel:
            raise RuntimeError("not support per channel")
        else:
            self.fake_quant = P.FakeQuantWithMinMax(num_bits=self.num_bits,
                                                    ema=self.ema,
                                                    ema_decay=self.ema_decay,
                                                    quant_delay=self.quant_delay,
                                                    symmetric=self.symmetric,
                                                    narrow_range=self.narrow_range,
                                                    training=training)
            if (in_channel % 16) == 0:
                self.ema_update = FakeQuantWithMinMaxUpdate5D(num_bits=self.num_bits,
                                                              ema=self.ema,
                                                              ema_decay=self.ema_decay,
                                                              quant_delay=self.quant_delay,
                                                              symmetric=self.symmetric,
                                                              narrow_range=self.narrow_range,
                                                              training=training)
            else:
                self.ema_update = FakeQuantWithMinMaxUpdate4D(num_bits=self.num_bits,
                                                              ema=self.ema,
                                                              ema_decay=self.ema_decay,
                                                              quant_delay=self.quant_delay,
                                                              symmetric=self.symmetric,
                                                              narrow_range=self.narrow_range,
                                                              training=training)

        if isinstance(min_init, Parameter):
            self.min = min_init
            self.max = max_init
        else:
            self.min = Parameter(Tensor(np.array([min_init]).astype(np.float32)), name='quant_min', requires_grad=False)
            self.max = Parameter(Tensor(np.array([max_init]).astype(np.float32)), name='quant_max', requires_grad=False)
        self.reduce_min = P.ReduceMin()
        self.reduce_max = P.ReduceMax()

    def extend_repr(self):
        s = 'min_init={}, max_init={}, ema={}, ema_decay={},  per_channel={}, channel_size={}, quant_delay={}'.format(
            self.min_init, self.max_init, self.ema, self.ema_decay, self.per_channel, self.channel_size,
            self.quant_delay)
        return s

    def construct(self, x, min, max):
        if self.training:
            min_up, max_up = self.ema_update(x, min, max)
            out = self.fake_quant(x, min_up, max_up)
            P.Assign()(self.min, min_up)
            P.Assign()(self.max, max_up)
        else:
            out = self.fake_quant(x, min, max)
        return out


class FakeQuantWithMinMax(Cell):
    r"""
    Aware Quantization training op. This OP provide Fake quantization observer function on data with min and max.

    Args:
        min_init (int, list): The dimension of channel or 1(layer). Default: -6.
        max_init (int, list): The dimension of channel or 1(layer). Default: 6.
        num_bits (int): Quantization number bit, support 4 and 8bit. Default: 8.
        ema (bool): Exponential Moving Average algorithm update min and max. Default: False.
        ema_decay (float): Exponential Moving Average algorithm parameter. Default: 0.9999.
        per_channel (bool): Quantization by layer or channel. Default: False.
        channel_size (int): declarate the min and max channel size, Default: 1.
        quant_delay (int): Quantization delay parameters according by global step. Default: 0.
        symmetric (bool): Quantization algorithm use symmetric or not. Default: False.
        narrow_range (bool): Quantization algorithm use narrow range or not. Default: False.

    Inputs:
        - **x** (Tensor) - The input of FakeQuantWithMinMax.

    Outputs:
        Tensor, with the same type and shape as the `x`.

    """

    def __init__(self,
                 in_channel,
                 min_init=-6,
                 max_init=6,
                 num_bits=8,
                 ema=False,
                 ema_decay=0.999,
                 per_channel=False,
                 channel_size=1,
                 quant_delay=0,
                 symmetric=False,
                 narrow_range=False,
                 training=True):
        """init FakeQuantWithMinMax layer"""
        super(FakeQuantWithMinMax, self).__init__()

        self.min_init = min_init
        self.num_bits = num_bits
        self.max_init = max_init
        self.ema = ema
        self.ema_decay = ema_decay
        self.per_channel = per_channel
        self.channel_size = channel_size
        self.quant_delay = quant_delay
        self.symmetric = symmetric
        self.narrow_range = narrow_range
        self.training = training

        if per_channel:
            min_array = np.array([self.min_init for i in range(
                0, self.channel_size)]).astype(np.float32)
            max_array = np.array([self.max_init for i in range(
                0, self.channel_size)]).astype(np.float32)
            self.min = Parameter(
                Tensor(min_array), name='quant_min', requires_grad=False)
            self.max = Parameter(
                Tensor(max_array), name='quant_max', requires_grad=False)
            self.fake_quant_train = P.FakeQuantWithMinMaxPerChannel(num_bits=self.num_bits,
                                                                    ema=self.ema,
                                                                    ema_decay=self.ema_decay,
                                                                    quant_delay=self.quant_delay,
                                                                    symmetric=self.symmetric,
                                                                    narrow_range=self.narrow_range,
                                                                    training=True)
            self.fake_quant_infer = P.FakeQuantWithMinMaxPerChannel(num_bits=self.num_bits,
                                                                    ema=self.ema,
                                                                    ema_decay=ema_decay,
                                                                    quant_delay=quant_delay,
                                                                    symmetric=self.symmetric,
                                                                    narrow_range=self.narrow_range,
                                                                    training=False)
        else:
            min_array = np.array([min_init]).reshape(1).astype(np.float32)
            max_array = np.array([max_init]).reshape(1).astype(np.float32)
            self.min = Parameter(
                Tensor(min_array), name='quant_min', requires_grad=False)
            self.max = Parameter(
                Tensor(max_array), name='quant_max', requires_grad=False)
            if context.get_context('device_target') == "Ascend":
                self.fake_quant_train = FakeQuantWithMinMaxD(in_channel,
                                                             num_bits=self.num_bits,
                                                             ema=self.ema,
                                                             ema_decay=self.ema_decay,
                                                             quant_delay=self.quant_delay,
                                                             symmetric=self.symmetric,
                                                             narrow_range=self.narrow_range,
                                                             training=True,
                                                             min_init=self.min,
                                                             max_init=self.max)
                self.fake_quant_infer = FakeQuantWithMinMaxD(in_channel,
                                                             num_bits=self.num_bits,
                                                             ema=self.ema,
                                                             ema_decay=self.ema_decay,
                                                             quant_delay=self.quant_delay,
                                                             symmetric=self.symmetric,
                                                             narrow_range=self.narrow_range,
                                                             training=False,
                                                             min_init=self.min,
                                                             max_init=self.max)
            else:
                self.fake_quant_train = P.FakeQuantWithMinMax(num_bits=self.num_bits,
                                                              ema=self.ema,
                                                              ema_decay=self.ema_decay,
                                                              quant_delay=self.quant_delay,
                                                              symmetric=self.symmetric,
                                                              narrow_range=self.narrow_range,
                                                              training=True)
                self.fake_quant_infer = P.FakeQuantWithMinMax(num_bits=self.num_bits,
                                                              ema=self.ema,
                                                              ema_decay=ema_decay,
                                                              quant_delay=quant_delay,
                                                              symmetric=self.symmetric,
                                                              narrow_range=self.narrow_range,
                                                              training=False)

    def extend_repr(self):
        s = 'min_init={}, max_init={}, ema={}, ema_decay={},  per_channel={}, channel_size={}, quant_delay={}'.format(
            self.min_init, self.max_init, self.ema, self.ema_decay, self.per_channel, self.channel_size,
            self.quant_delay)
        return s

    def construct(self, x):
        if self.training:
            out = self.fake_quant_train(x, self.min, self.max)
        else:
            out = self.fake_quant_infer(x, self.min, self.max)
        return out
