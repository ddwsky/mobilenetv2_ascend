from mindspore._checkparam import Validator as validator
from mindspore._checkparam import Rel, check_bool, check_int_positive, check_int
from mindspore.ops.primitive import PrimitiveWithInfer, prim_attr_register
from mindspore.ops import operations as P
from mindspore.common import dtype as mstype
from mindspore.ops._grad.grad_base import bprop_getters
from mindspore.ops.composite.multitype_ops.zeros_like_impl import zeros_like


class _BatchNormFold(PrimitiveWithInfer):
    @prim_attr_register
    def __init__(self, momentum=0.9, epsilon=1e-5, is_training=True, freeze_bn=0):
        self.momentum = validator.check_number_range('momentum', momentum, 0, 1, Rel.INC_BOTH, self.name)
        self.epsilon = validator.check_float_positive('epsilon', epsilon, self.name)
        self.is_training = validator.check_value_type('is_training', is_training, (bool,), self.name)
        self.freeze_bn = validator.check_value_type('freeze_bn', freeze_bn, (int,), self.name)
        self.data_format = "NCHW"
        self.init_prim_io_names(inputs=['x', 'x_sum', 'x_square_sum', 'mean', 'variance'],
                                outputs=['batch_mean', 'batch_std', 'running_mean', 'running_std',
                                         'mean_updated', 'variance_updated'])

    def infer_shape(self, x_shape, x_sum_shape, x_square_sum_shape, mean_shape, variance_shape):
        validator.check("mean shape", mean_shape, "gamma_shape", variance_shape, Rel.EQ, self.name)
        validator.check("mean_shape[0]", mean_shape[0], "input channel", x_shape[1], Rel.EQ, self.name)
        return x_shape, mean_shape, mean_shape, mean_shape, mean_shape, mean_shape, mean_shape

    def infer_dtype(self, x_type, x_sum_type, x_square_sum_type, mean_type, variance_type):
        validator.check("input type", x_type, "mean type", mean_type)
        validator.check("input type", x_type, "variance type", variance_type)
        args = {"x": x_type, "mean": mean_type, "variance": variance_type}
        validator.check_tensor_type_same(args, (mstype.float16, mstype.float32), self.name)
        return x_type, x_type, x_type, x_type, x_type, x_type, x_type


class _BatchNormFoldGrad(PrimitiveWithInfer):

    @prim_attr_register
    def __init__(self, epsilon=1e-5, is_training=True, freeze_bn=0):
        self.is_training = check_bool(is_training)
        self.freeze_bn = check_int(freeze_bn)
        self.epsilon = validator.check_float_positive('epsilon', epsilon, self.name)
        self.init_prim_io_names(inputs=['d_batch_mean', 'd_batch_std', 'x', 'batch_mean', 'batch_std'],
                                outputs=['dx'])

    def infer_shape(self, d_batch_mean_shape, d_batch_std_shape, x_shape, batch_mean_shape, batch_std_shape):
        validator.check("d_batch_mean shape", d_batch_mean_shape, "d_batch_std shape", d_batch_std_shape)
        validator.check("d_batch_mean shape", d_batch_mean_shape, "batch_mean shape", batch_mean_shape)
        validator.check("d_batch_mean shape", d_batch_mean_shape, "batch_std shape", batch_std_shape)
        validator.check("x_shape shape", d_batch_mean_shape[0], "input channel", x_shape[1])
        return x_shape

    def infer_dtype(self, d_batch_mean_type, d_batch_std_type, x_type, batch_mean_type, batch_std_type):
        validator.check("input type", x_type, "d_batch_mean type", d_batch_mean_type)
        validator.check("input type", x_type, "d_batch_std type", d_batch_std_type)
        validator.check("input type", x_type, "batch_mean type", batch_mean_type)
        validator.check("input type", x_type, "batch_std type", batch_std_type)
        args = {"input type": x_type}
        validator.check_tensor_type_same(args, (mstype.float16, mstype.float32), self.name)
        return x_type


@bprop_getters.register(_BatchNormFold)
def get_bprop_BatchNormFold(self):
    op = _BatchNormFoldGrad(self.epsilon, self.is_training, self.freeze_bn)

    def bprop(x, x_sum, x_square_sum, mean, variance, out, dout):
        dx = op(dout[1], dout[2], x, out[1], out[2])
        return dx, zeros_like(x_sum), zeros_like(x_square_sum), zeros_like(mean), zeros_like(variance)

    return bprop


class BNTrainingReduce(PrimitiveWithInfer):
    @prim_attr_register
    def __init__(self):
        self.init_prim_io_names(inputs=['x'],
                                outputs=['x_sum', 'x_square_sum'])

    def infer_shape(self, x_shape):
        return [x_shape[1]], [x_shape[1]]

    def infer_dtype(self, x_type):
        return x_type, x_type


@bprop_getters.register(BNTrainingReduce)
def get_bprop_BNTrainingReduce(self):
    def bprop(x, out, dout):
        return (zeros_like(x),)

    return bprop


class _BatchNormFold2(PrimitiveWithInfer):
    """
    Scale the bias with a correction factor to the long term statistics
    prior to quantization. This ensures that there is no jitter in the quantized bias
    due to batch to batch variation.

    Inputs:
        - **x** (Tensor)  - Tensor of shape :math:`(N, C)`.
        - **beta** (Tensor) - Tensor of shape :math:`(C,)`.
        - **gamma** (Tensor) - Tensor of shape :math:`(C,)`.
        - **batch_std** (Tensor) - Tensor of shape :math:`(C,)`.
        - **batch_mean** (Tensor) - Tensor of shape :math:`(C,)`.
        - **running_std** (Tensor) - Tensor of shape :math:`(C,)`.
        - **running_mean** (Tensor) - Tensor of shape :math:`(C,)`.
        - **global_step** (Tensor) - Tensor to record current global step.

    Outputs:
        - **y** (Tensor) - Tensor has the same shape as x.

    """
    channel = 1

    @prim_attr_register
    def __init__(self, freeze_bn=0):
        """init conv2d fold layer"""
        self.init_prim_io_names(inputs=['x', 'beta', 'gamma', 'batch_std', 'batch_mean', 'running_std'],
                                outputs=['y'])

    def infer_shape(self, x_shape, beta_shape, gamma_shape, batch_std_shape, running_std_shape, batch_mean_shape):
        validator.check("batch_std shape", batch_std_shape, "running_std shape", running_std_shape, Rel.EQ, self.name)
        validator.check("batch_std shape", batch_std_shape, "batch_mean shape", batch_mean_shape, Rel.EQ, self.name)
        validator.check("batch_std shape", batch_std_shape, "beta shape", beta_shape, Rel.EQ, self.name)
        validator.check("batch_std shape", batch_std_shape, "batch_mean shape", gamma_shape, Rel.EQ, self.name)
        validator.check("batch_std_shape[0]", batch_std_shape[0], "x_shape channel size", x_shape[self.channel],
                        Rel.EQ, self.name)
        return x_shape

    def infer_dtype(self, x_type, beta_type, gamma_type, batch_std_type, running_std_type, batch_mean_type):
        args = {"batch_std": batch_std_type, "running_std": running_std_type, "batch_mean": batch_mean_type,
                "beta": beta_type, "gamma": gamma_type, "x": x_type}
        validator.check_tensor_type_same(args, (mstype.float16, mstype.float32), self.name)
        return x_type


class _BatchNormFold2Grad(PrimitiveWithInfer):
    """Performs grad of CorrectionAddGrad operation."""
    channel = 1

    @prim_attr_register
    def __init__(self, freeze_bn=False):
        """init MulFold layer"""
        self.freeze_bn = freeze_bn
        self.init_prim_io_names(
            inputs=['dout', 'dout_reduce', 'dout_x_reduce', 'gamma', 'batch_std', 'batch_mean', 'running_std'],
            outputs=['d_batch_std', 'd_batch_mean', 'd_gamma', 'dx'])

    def infer_shape(self, dout_shape, dout_reduce_shape, dout_x_reduce_shape, gamma_shape, batch_std_shape,
                    batch_mean_shape, running_std_shape):
        validator.check("batch_std shape", batch_std_shape, "batch_mean shape", batch_mean_shape, Rel.EQ, self.name)
        validator.check("batch_std shape", batch_std_shape, "running_std shape", running_std_shape, Rel.EQ, self.name)
        validator.check("batch_std shape", batch_std_shape, "gamma shape", gamma_shape, Rel.EQ, self.name)
        validator.check("batch_std size", batch_std_shape[0], "dout channel size", dout_shape[self.channel],
                        Rel.EQ, self.name)
        return gamma_shape, gamma_shape, gamma_shape, dout_shape

    def infer_dtype(self, dout_type, dout_reduce_type, dout_x_reduce_type, gamma_type, batch_std_type,
                    batch_mean_type, running_std_type):
        validator.check("batch_std type", batch_std_type,
                        "batch_mean type", batch_mean_type)
        validator.check("batch_std type", batch_std_type,
                        "gamma type", gamma_type)
        validator.check("batch_std type", batch_std_type,
                        "running_std type", running_std_type)
        validator.check("batch_std_type", batch_std_type,
                        "dout type", dout_type)
        args = {"batch_std": batch_std_type, "batch_mean": batch_mean_type, "gamma": gamma_type,
                "running_std": running_std_type, "dout": dout_type}
        validator.check_tensor_type_same(args, (mstype.float16, mstype.float32), self.name)
        return gamma_type, gamma_type, gamma_type, gamma_type


class _BatchNormFold2GradReduce(PrimitiveWithInfer):
    """Performs grad of CorrectionAddGrad operation."""
    channel = 1

    @prim_attr_register
    def __init__(self, freeze_bn=False):
        """init MulFold layer"""
        self.freeze_bn = freeze_bn
        self.init_prim_io_names(inputs=['dout', 'x'],
                                outputs=['dout_reduce', 'dout_x_reduce'])

    def infer_shape(self, dout_shape, x_shape):
        validator.check("dout shape", dout_shape, "x shape", x_shape, Rel.EQ, self.name)
        return (dout_shape[self.channel],), (dout_shape[self.channel],)

    def infer_dtype(self, dout_type, x_type):
        validator.check("dout type", dout_type, "x type", x_type)
        return dout_type, dout_type


@bprop_getters.register(_BatchNormFold2)
def get_bprop_batchnorm_fold2(self):
    """Generate bprop for _BatchNormFold2"""
    op_reduce = _BatchNormFold2GradReduce(freeze_bn=self.freeze_bn)
    op_f = _BatchNormFold2Grad(freeze_bn=self.freeze_bn)

    def bprop(x, beta, gamma, batch_std, batch_mean, running_std, out, dout):
        dout_reduce, dout_x_reduce = op_reduce(dout, x)
        d_batch_std, d_batch_mean, d_gamma, d_x = op_f(dout, dout_reduce, dout_x_reduce, gamma, batch_std,
                                                       batch_mean, running_std)
        return d_x, dout_reduce, d_gamma, d_batch_std, d_batch_mean, zeros_like(running_std)

    return bprop


class FakeQuantWithMinMaxUpdate4D(PrimitiveWithInfer):
    r"""
    Simulate the quantize and dequantize operations in training time.

    Args:
        num_bits (int) : Number bits for aware quantilization. Default: 8.
        ema (bool): Use EMA algorithm update value min and max. Default: False.
        ema_decay (int) : EMA algorithm decay parameter. Default: 0.999.
        quant_delay (int): Quantilization delay parameter. Before delay step in training time not update
            simulate aware quantize funcion. After delay step in training time begin simulate the aware
            quantize funcion. Default: 0.
        symmetric (bool): Quantization algorithm use symmetric or not. Default: False.
        narrow_range (bool): Quantization algorithm use narrow range or not. Default: False.
        training (bool): Training the network or not. Default: True.

    Inputs:
        - **x** (Tensor) : float32 Tensor representing the shape of the output tensor.
        - **min** (Tensor) : Value of the min range of the input data x.
        - **max** (Tensor) : Value of the max range of the input data x.

    Outputs:
        - Tensor: Simulate quantize tensor of x.

    Examples:
        >>> input_tensor = Tensor(np.random.rand(3, 16, 5, 5), mstype.float32)
        >>> min_tensor = Tensor(np.array([-6]), mstype.float32)
        >>> max_tensor = Tensor(np.array([6]), mstype.float32)
        >>> output_tensor = P.FakeQuantWithMinMax(num_bits=8)(input_tensor, min_tensor, max_tensor)
    """
    support_quant_bit = [4, 7, 8]

    @prim_attr_register
    def __init__(self, num_bits=8, ema=False, ema_decay=0.999, quant_delay=0, symmetric=False, narrow_range=False,
                 training=True):
        """init FakeQuantWithMinMax OP"""
        if num_bits not in self.support_quant_bit:
            raise ValueError(f"For '{self.name}' attr \'num_bits\' is not support.")
        if ema and not ema_decay:
            raise ValueError(f"For '{self.name}' attr \'ema\' and \'ema_decay\' should set together.")

        self.ema = validator.check_value_type('ema', ema, (bool,), self.name)
        self.symmetric = validator.check_value_type('symmetric', symmetric, (bool,), self.name)
        self.narrow_range = validator.check_value_type('narrow_range', narrow_range, (bool,), self.name)
        self.training = validator.check_value_type('training', training, (bool,), self.name)
        self.ema_decay = validator.check_number_range('ema_decay', ema_decay, 0, 1, Rel.INC_BOTH, self.name)
        self.num_bits = validator.check_integer('num_bits', num_bits, 0, Rel.GT, self.name)
        self.quant_delay = validator.check_value_type('quant_delay', quant_delay, (int,), self.name)
        self.init_prim_io_names(inputs=['x', 'min', 'max'],
                                outputs=['min_up', 'max_up'])

    def infer_shape(self, x_shape, min_shape, max_shape):
        validator.check_integer("x rank", len(x_shape), 1, Rel.GT, self.name)
        validator.check("min shape", min_shape, "max shape", max_shape, Rel.EQ, self.name)
        validator.check_integer("min rank", len(min_shape), 1, Rel.EQ, self.name)
        return min_shape, max_shape

    def infer_dtype(self, x_type, min_type, max_type):
        valid_types = (mstype.float16, mstype.float32)
        validator.check_tensor_type_same({"x": x_type}, valid_types, self.name)
        validator.check_tensor_type_same({"min": min_type}, valid_types, self.name)
        validator.check_tensor_type_same({"max": max_type}, valid_types, self.name)
        return min_type, max_type


@bprop_getters.register(FakeQuantWithMinMaxUpdate4D)
def get_bprop_fakequant_with_minmax_update4d(self):
    """Generate bprop for FakeQuantWithMinMaxUpdate"""

    def bprop(x, x_min, x_max, out, dout):
        return zeros_like(x), zeros_like(x_min), zeros_like(x_max)

    return bprop


class FakeQuantWithMinMaxUpdate5D(PrimitiveWithInfer):
    r"""
    Simulate the quantize and dequantize operations in training time.

    Args:
        num_bits (int) : Number bits for aware quantilization. Default: 8.
        ema (bool): Use EMA algorithm update value min and max. Default: False.
        ema_decay (int) : EMA algorithm decay parameter. Default: 0.999.
        quant_delay (int): Quantilization delay parameter. Before delay step in training time not update
            simulate aware quantize funcion. After delay step in training time begin simulate the aware
            quantize funcion. Default: 0.
        symmetric (bool): Quantization algorithm use symmetric or not. Default: False.
        narrow_range (bool): Quantization algorithm use narrow range or not. Default: False.
        training (bool): Training the network or not. Default: True.

    Inputs:
        - **x** (Tensor) : float32 Tensor representing the shape of the output tensor.
        - **min** (Tensor) : Value of the min range of the input data x.
        - **max** (Tensor) : Value of the max range of the input data x.

    Outputs:
        - Tensor: Simulate quantize tensor of x.

    Examples:
        >>> input_tensor = Tensor(np.random.rand(3, 16, 5, 5), mstype.float32)
        >>> min_tensor = Tensor(np.array([-6]), mstype.float32)
        >>> max_tensor = Tensor(np.array([6]), mstype.float32)
        >>> output_tensor = P.FakeQuantWithMinMax(num_bits=8)(input_tensor, min_tensor, max_tensor)
    """
    support_quant_bit = [4, 7, 8]

    @prim_attr_register
    def __init__(self, num_bits=8, ema=False, ema_decay=0.999, quant_delay=0, symmetric=False, narrow_range=False,
                 training=True):
        """init FakeQuantWithMinMax OP"""
        if num_bits not in self.support_quant_bit:
            raise ValueError(f"For '{self.name}' attr \'num_bits\' is not support.")
        if ema and not ema_decay:
            raise ValueError(f"For '{self.name}' attr \'ema\' and \'ema_decay\' should set together.")

        self.ema = validator.check_value_type('ema', ema, (bool,), self.name)
        self.symmetric = validator.check_value_type('symmetric', symmetric, (bool,), self.name)
        self.narrow_range = validator.check_value_type('narrow_range', narrow_range, (bool,), self.name)
        self.training = validator.check_value_type('training', training, (bool,), self.name)
        self.ema_decay = validator.check_number_range('ema_decay', ema_decay, 0, 1, Rel.INC_BOTH, self.name)
        self.num_bits = validator.check_integer('num_bits', num_bits, 0, Rel.GT, self.name)
        self.quant_delay = validator.check_value_type('quant_delay', quant_delay, (int,), self.name)
        self.init_prim_io_names(inputs=['x', 'min', 'max'],
                                outputs=['min_up', 'max_up'])

    def infer_shape(self, x_shape, min_shape, max_shape):
        validator.check_integer("x rank", len(x_shape), 1, Rel.GT, self.name)
        validator.check("min shape", min_shape, "max shape", max_shape, Rel.EQ, self.name)
        validator.check_integer("min rank", len(min_shape), 1, Rel.EQ, self.name)
        return min_shape, max_shape

    def infer_dtype(self, x_type, min_type, max_type):
        valid_types = (mstype.float16, mstype.float32)
        validator.check_tensor_type_same({"x": x_type}, valid_types, self.name)
        validator.check_tensor_type_same({"min": min_type}, valid_types, self.name)
        validator.check_tensor_type_same({"max": max_type}, valid_types, self.name)
        return min_type, max_type


@bprop_getters.register(FakeQuantWithMinMaxUpdate5D)
def get_bprop_fakequant_with_minmax_update5d(self):
    """Generate bprop for FakeQuantWithMinMaxUpdate"""

    def bprop(x, x_min, x_max, out, dout):
        return zeros_like(x), zeros_like(x_min), zeros_like(x_max)

    return bprop


############################################################
class FakeQuantPerChannelAscend(PrimitiveWithInfer):
    r"""
    Simulate the quantize and dequantize operations in training time base on per channel.

    Args:
        num_bits (int) : Number bits to quantilization. Default: 8.
        symmetric (bool): Quantization algorithm use symmetric or not. Default: False.
        narrow_range (bool): Quantization algorithm use narrow range or not. Default: False.
        channel_axis (int): Tensor Channel axis. Default: 1.

    Inputs:
        - **x** (Tensor) : 4-D float32 Tensor representing the shape of the output tensor.
        - **min** (int, float) : Value of the min range of the input data.
        - **max** (int, float) : Value of the max range of the input data.

    Outputs:
        - Tensor, has the same type as input.

    Examples:
        >>> fake_quant = P.FakeQuantWithMinMaxPerChannel()
        >>> x = Tensor(np.random.rand((256, 24, 56, 56), mindspore.float32)
        >>> min = Tensor(np.random.uniform(-1, 1, size=24), mindspore.float32)
        >>> max = Tensor(np.random.uniform(-1, 1, size=24), mindspore.float32)
        >>> result = fake_quant(x, min, max)
    """
    support_quant_bit = [4, 7, 8]

    @prim_attr_register
    def __init__(self, num_bits=8, symmetric=False, narrow_range=False, channel_axis=1):
        """init FakeQuantPerChannel OP for backend Ascend"""
        if num_bits not in self.support_quant_bit:
            raise ValueError(f"For '{self.name}' Attr \'num_bits\' is not support.")

        self.symmetric = validator.check_value_type(
            'symmetric', symmetric, (bool,), self.name)
        self.narrow_range = validator.check_value_type(
            'narrow_range', narrow_range, (bool,), self.name)
        self.num_bits = validator.check_integer(
            'num_bits', num_bits, 0, Rel.GT, self.name)
        self.channel_axis = validator.check_integer(
            'channel_axis', channel_axis, 0, Rel.GE, self.name)
        self.init_prim_io_names(inputs=['x', 'min', 'max'], outputs=['out'])

    def infer_shape(self, x_shape, min_shape, max_shape):
        validator.check_integer("x rank", len(x_shape), 1, Rel.GE, self.name)
        validator.check_integer(
            "min shape[0]", min_shape[0], x_shape[self.channel_axis], Rel.EQ, self.name)
        validator.check_integer(
            "max shape[0]", max_shape[0], x_shape[self.channel_axis], Rel.EQ, self.name)
        return x_shape

    def infer_dtype(self, x_type, min_type, max_type):
        valid_types = (mstype.float16, mstype.float32)
        validator.check_tensor_type_same({"x": x_type}, valid_types, self.name)
        validator.check_tensor_type_same(
            {"min": min_type}, valid_types, self.name)
        validator.check_tensor_type_same(
            {"max": max_type}, valid_types, self.name)
        return x_type


class FakeQuantPerChannelGradAscend(PrimitiveWithInfer):
    r"""
    Performs grad of FakeQuantPerChannelAscend operation.

    Examples:
        >>> fake_grad = P.FakeQuantPerChannelGradAscend()
        >>> input_x = Tensor(np.random.randint(-4, 4, (2, 3, 4)), mindspore.float32)
        >>> dout = Tensor(np.random.randint(-2, 2, (2, 3, 4)), mindspore.float32)
        >>> _min = Tensor(np.random.randint(-8, 2, (2, 3, 4)), mindspore.float32)
        >>> _max = Tensor(np.random.randint(-2, 8, (2, 3, 4)), mindspore.float32)
        >>> result = fake_grad(dout, input_x, _min, _max)
    """
    support_quant_bit = [4, 7, 8]

    @prim_attr_register
    def __init__(self, num_bits=8, symmetric=False, narrow_range=False, channel_axis=1):
        """init FakeQuantPerChannel OP"""
        if num_bits not in self.support_quant_bit:
            raise ValueError(
                f"For '{self.name}' Attr \'num_bits\' is not support.")

        self.symmetric = validator.check_value_type(
            'symmetric', symmetric, (bool,), self.name)
        self.narrow_range = validator.check_value_type(
            'narrow range', narrow_range, (bool,), self.name)
        self.num_bits = validator.check_integer(
            'num bits', num_bits, 0, Rel.GT, self.name)
        self.channel_axis = validator.check_integer(
            'channel axis', channel_axis, 0, Rel.GE, self.name)
        self.init_prim_io_names(
            inputs=['dout', 'x', 'min', 'max'], outputs=['dx'])

    def infer_shape(self, dout_shape, x_shape, min_shape, max_shape):
        validator.check("dout shape", dout_shape, "x shape", x_shape)
        validator.check("min shape", min_shape, "max shape", max_shape)
        return dout_shape

    def infer_dtype(self, dout_type, x_type, min_type, max_type):
        valid_types = (mstype.float16, mstype.float32)
        validator.check_tensor_type_same(
            {"dout": dout_type}, valid_types, self.name)
        validator.check_tensor_type_same(
            {"x": x_type}, valid_types, self.name)
        validator.check_tensor_type_same(
            {"min": min_type}, valid_types, self.name)
        validator.check_tensor_type_same(
            {"max": max_type}, valid_types, self.name)
        return dout_type


@bprop_getters.register(FakeQuantPerChannelAscend)
def get_bprop_fakequant_with_minmax_perchannel(self):
    """Generate bprop for FakeQuantPerChannel for backend Ascend"""
    op = FakeQuantPerChannelGradAscend(num_bits=self.num_bits,
                                       symmetric=self.symmetric,
                                       narrow_range=self.symmetric,
                                       channel_axis=self.channel_axis)

    def bprop(x, x_min, x_max, out, dout):
        dx = op(dout, x, x_min, x_max)
        return dx, zeros_like(x_min), zeros_like(x_max)

    return bprop


class FakeQuantPerChannelUpdateAscend(PrimitiveWithInfer):
    r"""
    Simulate the quantize and dequantize operations in training time.

    Args:
        num_bits (int) : Number bits for aware quantilization. Default: 8.
        ema (bool): Use EMA algorithm update value min and max. Default: False.
        ema_decay (int) : EMA algorithm decay parameter. Default: 0.999.
        symmetric (bool): Quantization algorithm use symmetric or not. Default: False.
        narrow_range (bool): Quantization algorithm use narrow range or not. Default: False.
        training (bool): Training the network or not. Default: True.

    Inputs:
        - **x** (Tensor) : float32 Tensor representing the shape of the output tensor.
        - **min** (Tensor) : Value of the min range of the input data x.
        - **max** (Tensor) : Value of the max range of the input data x.

    Outputs:
        - Tensor: Simulate quantize tensor of x.

    Examples:
        >>> x = Tensor(np.random.rand(3, 16, 5, 5), mstype.float32)
        >>> min = Tensor(np.random.uniform(-1, 1, size=16), mstype.float32)
        >>> max = Tensor(np.random.uniform(-1, 1, size=16), mstype.float32)
        >>> output_tensor = P.FakeQuantWithMinMax(num_bits=8)(x, min, max)
    """
    support_quant_bit = [4, 7, 8]

    @prim_attr_register
    def __init__(self, num_bits=8, ema=False, ema_decay=0.999, symmetric=False, narrow_range=False,
                 training=True, channel_axis=1):
        """init FakeQuantPerChannelUpdateAscend OP"""
        if num_bits not in self.support_quant_bit:
            raise ValueError(
                f"For '{self.name}' attr \'num_bits\' is not support.")
        if ema and not ema_decay:
            raise ValueError(
                f"For '{self.name}' attr \'ema\' and \'ema_decay\' should set together.")

        self.ema = validator.check_value_type('ema', ema, (bool,), self.name)
        self.symmetric = validator.check_value_type(
            'symmetric', symmetric, (bool,), self.name)
        self.narrow_range = validator.check_value_type(
            'narrow_range', narrow_range, (bool,), self.name)
        self.training = validator.check_value_type(
            'training', training, (bool,), self.name)
        self.ema_decay = validator.check_number_range(
            'ema_decay', ema_decay, 0, 1, Rel.INC_BOTH, self.name)
        self.num_bits = validator.check_integer(
            'num_bits', num_bits, 0, Rel.GT, self.name)
        self.channel_axis = validator.check_integer(
            'channel axis', channel_axis, 0, Rel.GE, self.name)
        self.init_prim_io_names(inputs=['x', 'min', 'max'], outputs=['min_up', 'max_up'])

    def infer_shape(self, x_shape, min_shape, max_shape):
        validator.check_integer("x rank", len(x_shape), 1, Rel.GT, self.name)
        validator.check("min shape", min_shape, "max shape", max_shape, Rel.EQ, self.name)
        validator.check_integer("min rank", len(min_shape), 1, Rel.EQ, self.name)
        return min_shape, max_shape

    def infer_dtype(self, x_type, min_type, max_type):
        valid_types = (mstype.float16, mstype.float32)
        validator.check_tensor_type_same(
            {"x": x_type}, valid_types, self.name)
        validator.check_tensor_type_same(
            {"min": min_type}, valid_types, self.name)
        validator.check_tensor_type_same(
            {"max": max_type}, valid_types, self.name)
        return min_type, max_type


@bprop_getters.register(FakeQuantPerChannelUpdateAscend)
def get_bprop_fakequant_with_minmax_update(self):
    """Generate bprop for FakeQuantWithMinMaxUpdate"""

    def bprop(x, x_min, x_max, out, dout):
        return zeros_like(x), zeros_like(x_min), zeros_like(x_max)

    return bprop
